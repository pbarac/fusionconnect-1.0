global class CS_AccessIpTypeLookup extends cscfga.ALookupSearch {

    public static final String REQUIRED_ATTRIBUTES = CS_Constants.REQUIRED_ATTRIBUTES_CS_AccessIpTypeLookup;

    // Attribute names used in the WHERE clause of SOQL query, JSON notation, e.g. '["Attribute1","Attribute2"]'
    public override String getRequiredAttributes() {
        return REQUIRED_ATTRIBUTES;
    }  
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
        List<cspmb__Price_Item__c> commProdLst = new List<cspmb__Price_Item__c>();

        List<AggregateResult> agr = [SELECT min(Id) cpid, fc_IP_type__c FROM cspmb__Price_Item__c 
                        WHERE cspmb__Product_Definition_Name__c =: CS_Constants.Product_Definition_Access group by fc_IP_type__c];
        
        for (AggregateResult ar : agr){
            string ipType = string.Valueof(ar.get('fc_IP_type__c'));
            if(!String.isBlank(ipType)){
                cspmb__Price_Item__c cp = new cspmb__Price_Item__c(Id = string.Valueof(ar.get('cpid')), Name = ipType);
                commProdLst.add(cp);
            }
        }

        return commProdLst;
    }

}