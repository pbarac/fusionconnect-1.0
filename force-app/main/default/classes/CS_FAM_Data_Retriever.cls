global with sharing class CS_FAM_Data_Retriever implements cssmgnt.RemoteActionDataProvider {

	/**
	 * Returns
	 */
	@RemoteAction
	global static Map<String, Object> getData(Map<String, Object> inputMap) {

		String basketId = inputMap.get('basketId').toString();
		cscfga__Product_Basket__c basket = [
			SELECT Opportunity_Linked_Account__c, cscfga__Opportunity__c
			FROM cscfga__Product_Basket__c
			WHERE id = :basketId
			LIMIT 1
		][0];
	
		Opportunity opp = getOpportunity(basket.cscfga__Opportunity__c);

		String accountId = basket.Opportunity_Linked_Account__c;

		String selectedPRG = opp.Selected_Pricebook__c;
		String selectedFa = opp.Selected_Frame_Agreement__c;

		cspmb__Pricing_Rule_Group__c prg = null;
		if (selectedPRG != null) {
			prg = [SELECT Name FROM cspmb__Pricing_Rule_Group__c WHERE cspmb__pricing_rule_group_code__c = :selectedPRG LIMIT 1][0];
		}

		String prgName = '';
		if (prg != null) {
			prgName = prg.Name;
		}

		csconta__Frame_Agreement__c fa = null;
		if (selectedFa != null) {
			fa = [SELECT csconta__Agreement_Name__c, PRGs__c FROM csconta__Frame_Agreement__c WHERE Id = :selectedFa LIMIT 1][0];
		}

		String agreementName = '';
		String faprgs = '';
		if (fa != null) {
			agreementName = fa.csconta__Agreement_Name__c;
			faprgs = fa.PRGs__c;
		}

		return new Map<String, Object>{
			'accountId' => accountId,

			'catalogueName' => prgName,
			'selectedPRG' => selectedPRG,

			'faName' => agreementName,
			'selectedFA' => faprgs
		};
	}

	private static Opportunity getOpportunity(String opportunityId) {
		Opportunity opp = [
			SELECT Id, Selected_Pricebook__c, Selected_Frame_Agreement__c, AccountId
			FROM Opportunity
			WHERE Id = :opportunityId
			LIMIT 1
		][0];

		return opp;
	}
}