@isTest
private class CS_AccessCarrierBandwidthLookup_Test {
    
    @isTest static void testDoLookupSearch() {
        
        //create a commercial product
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_IP_type__c = 'Static';
        cpi.fc_tech_type__c = 'WiFi';
        cpi.fc_on_network__c = 'N';
        cpi.fc_vendor__c = 'Meraki';
        cpi.fc_down_speed_str__c = '1.5 Gb';
        insert cpi;    
       

        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('IP Type Value', 'Static');
        searchFields.put('Tech Type Value', 'WiFi');
        searchFields.put('Network Type Value', 'N');
        searchFields.put('Access Carrier Value', 'Meraki');

        Test.startTest();
        CS_AccessCarrierBandwidthLookup testObject = new CS_AccessCarrierBandwidthLookup();
        testObject.getRequiredAttributes();
        Object[] carrierBCPLst = testObject.doLookupSearch(searchFields, null, null, null, null);
        Test.stopTest();

        System.assertNotEquals(carrierBCPLst.size(), null, 'Carrier Bandwidth Values are not retrieved');
        cpi = (cspmb__Price_Item__c) carrierBCPLst[0];
        System.assertEquals(cpi.name, '1.5 Gb', 'Carrier Bandwidth is not correct');
    }
}