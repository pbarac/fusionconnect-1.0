public with sharing class CS_AddressValidationController {

    @AuraEnabled
    public static String getAddressRecords(String recordId){
        List<CS_AddressWrapper> addressRecords = new List<CS_AddressWrapper>();
        Map<String, String> returnMap = new Map<String, String>();

        cscfga__Product_Basket__c basket = [SELECT id,csbb__Account__c FROM cscfga__Product_Basket__c WHERE id=: recordId];
        String accountId = basket.csbb__Account__c;

        List<cscrm__Address__c> addressRecordsList = [SELECT Id,
                Name,
                cscrm__Street__c,
                cscrm__Street_Name__c,
                cscrm__City__c,
                cscrm__State_Province__c,
                cscrm__Zip_Postal_Code__c,
                cscrm__Country__c
        FROM cscrm__Address__c
        WHERE cscrm__Account__c =: accountId
        order by CreatedDate DESC
        ];

        if(!addressRecordsList.isEmpty()){

            checkAddressRecord(addressRecordsList);
            addressRecords = createAddressWrapperRecords(addressRecordsList);

        }

        returnMap.put('ADDRESS_RECORDS',JSON.serialize(addressRecords));
        returnMap.put('ACCOUNT_ID', accountId);
        
        return JSON.serialize(returnMap);
    }



    @AuraEnabled
    public static String getFieldsFromFieldSet(String objectApiName, String fieldSetName){
        if(!String.isEmpty(objectApiName) && !String.isEmpty(fieldSetName)){   
            Map<String, String> returnMap = new Map<String, String>();

            //get fields from FieldSet
            SObject sObj = (SObject)(Type.forName('Schema.'+ objectApiName)?.newInstance());
            List<Schema.FieldSetMember> lstFSMember = 
                sObj?.getSObjectType()?.getDescribe()?.fieldSets.getMap().get(fieldSetName)?.getFields();
                
            returnMap.put('FIELD_LIST',JSON.serialize(lstFSMember));
            returnMap.put('OBJECT_LABEL', sObj?.getSObjectType()?.getDescribe()?.getLabel());

            return JSON.serialize(returnMap);
        }                                                                 
        return null;
    }

    @AuraEnabled
    public static String saveAddressRecords(String base64Data, String accountId){
        Map<String, String> returnMap = new Map<String, String>();
        List<CS_AddressWrapper> addressRecordsBulk = (List<CS_AddressWrapper>)JSON.deserialize(base64Data,List<CS_AddressWrapper>.class);
        List<CS_AddressWrapper> addressWrapperRecords = new List<CS_AddressWrapper>();

        List<cscrm__Address__c> newAddressRecords = CS_AddressDuplicateCheck.filterDuplicateAddresses(addressRecordsBulk, accountId);

        if(!newAddressRecords.isEmpty()){
            try{
                insert newAddressRecords;

                addressWrapperRecords = createAddressWrapperRecords(newAddressRecords);
                returnMap.put('STATUS','200');
                returnMap.put('ADDRESS_RECORDS', JSON.serialize(addressWrapperRecords));
            }catch(Exception e){
                System.debug(LoggingLevel.ERROR, '[CS_AddressValidationController] - saveAddressRecords - ' + e.getMessage());
            }
        }else{
            returnMap.put('STATUS','500');
            returnMap.put('ADDRESS_RECORDS', JSON.serialize(addressWrapperRecords));
        }

        return JSON.serialize(returnMap);
      
    }

    private static List<CS_AddressWrapper> createAddressWrapperRecords(List<cscrm__Address__c> addressRecordsList){
        List<CS_AddressWrapper> addressRecords = new List<CS_AddressWrapper>();
        
        for(Integer i = 0; i < addressRecordsList.size(); i++){
    
                CS_AddressWrapper el = new CS_AddressWrapper();
                el.addressLookup = addressRecordsList[i].id;
                el.name = addressRecordsList[i].Name;
                el.street = addressRecordsList[i].cscrm__Street__c;
                el.streetName = addressRecordsList[i].cscrm__Street_Name__c;
                el.city = addressRecordsList[i].cscrm__City__c;
                el.state_province = addressRecordsList[i].cscrm__State_Province__c;
                el.zipCode = addressRecordsList[i].cscrm__Zip_Postal_Code__c;
                el.country = addressRecordsList[i].cscrm__Country__c;
                el.validationFlag = false;
                el.validationOverrideFlag = false;
                addressRecords.add(el);
    
            }

            return addressRecords;
    }
    


    //after we make these fields mandatory --> remove
    private static void checkAddressRecord(List<cscrm__Address__c> addresses){
        for( cscrm__Address__c add : addresses ){

            if(String.isBlank(add.Name)){
                add.Name = '';
            }
    
            if(String.isBlank(add.cscrm__Street__c)){
                add.cscrm__Street__c = '';
            }
    
            if(String.isBlank(add.cscrm__Street_Name__c)){
                add.cscrm__Street_Name__c = '';
            }
    
            if(String.isBlank(add.cscrm__City__c)){
                add.cscrm__City__c = '';
            }
            
            if(String.isBlank(add.cscrm__State_Province__c)){
                add.cscrm__State_Province__c = '';
            }
    
            if(String.isBlank(add.cscrm__Zip_Postal_Code__c)){
                add.cscrm__Zip_Postal_Code__c = '';
            }
    
            if(String.isBlank(add.cscrm__Country__c)){
                add.cscrm__Country__c = '';
            }

        }
  
    }
}