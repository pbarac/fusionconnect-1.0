/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANVendorLookup_Test - Test coverage for class CS_SDWANVendorLookup
*/
@isTest 
public class CS_SDWANVendorLookup_Test {

    @isTest public static void doLookupSearchTest(){
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'SD-WAN';
        insert cpi;
        
        Map<String,String> searchFields = new Map <String, String>();
        searchFields.put('High Availability Value', 'No');
        searchFields.put('SDWAN Type Value', 'WiFi');
        searchFields.put('Prioritized bandwidth Value', '100 Mbps');
        searchFields.put('Advanced Security Value', 'No');

        Test.startTest();
        CS_SDWANVendorLookup testObject = new CS_SDWANVendorLookup();
        testObject.getRequiredAttributes();        
        Object[] vendorList = testObject.doLookupSearch(searchFields,null,null,null,null);
        Test.stopTest();
        
        System.assertNotEquals(vendorList.size(), null, 'Vendor Name Value not fetched');
    }
}