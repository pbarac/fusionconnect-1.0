/**
* Created by Fayaz Shaik
*/

global with sharing class CS_PREApiV6Pricings implements cssmgnt.RemoteActionDataProvider {
    
    @RemoteAction
    global static Map<String, Object> getData(Map<String, Object> inputMap) {
        System.debug('fayaz::--start');
        System.debug(inputMap);
        System.debug('fayaz::--end');
        
        String payload = inputMap.get('payload').toString();
        
        HttpResponse resp = CS_RequestUtil.sendRequest(
            'POST',
            '/pre/v6/pricings',
            payload
        );
        
        return new Map<String, Object>{
            'status' => resp.getStatus(),
                'statusCode' => resp.getStatusCode(),
                'body' => resp.getBody()
                };
                    
                    
                    
                    }
}