/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANPrioritizedBandwidthLookup - Lookup for Prioritized Bandwidth in solution management 
*/
global class CS_SDWANPrioritizedBandwidthLookup extends cscfga.ALookupSearch {

    public static final String REQUIRED_ATTRIBUTES = '["Id", "Name", "SDWAN Type Value"]';

    // Attribute names used in the WHERE clause of SOQL query, JSON notation, e.g. '["Attribute1","Attribute2"]'    
    public override String getRequiredAttributes() {
        return REQUIRED_ATTRIBUTES;
    }    
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){

        List<cspmb__Price_Item__c> commProdLst = new List<cspmb__Price_Item__c>();
        String techTypeValue = searchFields.get('SDWAN Type Value');
        
        List<AggregateResult> agr = [SELECT min(Id) cpid, fc_prioritized_bandwidth__c FROM cspmb__Price_Item__c 
                                      WHERE cspmb__Product_Definition_Name__c = 'SD-WAN' and fc_tech_type__c =: techTypeValue and fc_prioritized_bandwidth__c != null
                                       group by fc_prioritized_bandwidth__c order by fc_prioritized_bandwidth__c];
        
        for (AggregateResult ar : agr){
                cspmb__Price_Item__c cp = new cspmb__Price_Item__c(Id = string.Valueof(ar.get('cpid')), Name = string.Valueof(ar.get('fc_prioritized_bandwidth__c')), fc_prioritized_bandwidth__c = string.Valueof(ar.get('fc_prioritized_bandwidth__c')));
                commProdLst.add(cp);
        }
        return commProdLst;
    }
}