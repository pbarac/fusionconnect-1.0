global with sharing class CS_PREBasketUpdater implements cssmgnt.RemoteActionDataProvider {

	/**
	 * In order to be invoked from the Solution Console it implements the cssmgnt.RemoteActionDataProvider
	 * @param inputMap
	 * @example
	 * {
	 *	 [basketId]: 'prg1, prg2, prg3'
	 * }
	 */
	@RemoteAction
	global static Map<String, Object> getData(Map<String, Object> inputMap) {
		try {
			String basketId = (new List<String>(inputMap.keySet()))[0];

			cscfga__product_basket__c basket = [
				SELECT Id, csdiscounts__Applied_Promotions__c
				FROM cscfga__product_basket__c
				WHERE Id = :basketId
			];

			basket.csdiscounts__Applied_Promotions__c = (String) inputMap.get(basketId);

			update basket;

			return new Map<String, Object>{
				'status' => 'ok'
			};
		} catch (Exception e) {
			return new Map<String, Object>{
				'status' => 'error',
				'message' => e.getMessage()
			};
		}
	}
}