@isTest public with sharing class CS_RequestUtil_Test {
    @isTest public static void CS_RequestUtil_Test() {
        Test.startTest();

        HttpRequest req = new HttpRequest();
        HttpResponse response =  CS_RequestUtil.sendRequest('GET','https://example.com/example','{test}');
   
        Test.stopTest();

        System.assertNotEquals(response, null, 'Response not fetched');
    }
}