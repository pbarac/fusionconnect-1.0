@isTest public with sharing class CS_FAM_Data_Retriever_Test {
    @isTest public static void CS_FAM_Data_Retriever_Test(){
        Account acc = CS_TestDataFactory.createAccount(true);
        List<cscfga__Product_Basket__c> pbLst = CS_TestDataFactory.createBaskets(1, acc.Id, true);
        
        Map<String, Object> inputMap = new Map<String, Object>{
            'basketId' => pbLst[0].id
        };
        Test.startTest();
       
        Map<String, Object> testdata = CS_FAM_Data_Retriever.getData(inputMap);
       
        Test.stopTest();
       	
        System.assertNotEquals(testdata, null,'No Data Fetched');
          
    }
 }