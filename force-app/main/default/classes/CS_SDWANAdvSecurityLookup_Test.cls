/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANAdvSecurityLookup_Test - Test coverage for class CS_SDWANAdvSecurityLookup
*/
@isTest 
public class CS_SDWANAdvSecurityLookup_Test {

    @isTest public static void doLookupSearchTest(){
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'SD-WAN';
        insert cpi;
        
        Test.startTest();
        
        Map<String,String> searchFields = new Map <String, String>();
        searchFields.put('SDWAN Type Value', 'WiFi');
        searchFields.put('Prioritized bandwidth Value', '100 Mbps');
        
        CS_SDWANAdvSecurityLookup testObject = new CS_SDWANAdvSecurityLookup();
        testObject.getRequiredAttributes();                
        Object[] AdvList = testObject.doLookupSearch(searchFields,null,null,null,null);
        Test.stopTest();
        
        System.assertNotEquals(AdvList.size(), null, 'Advanced Security Value not fetched');
    }
}