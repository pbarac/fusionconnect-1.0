public with sharing class CS_AddressDuplicateCheck {

//method return list of non existing addresses
    public static List<cscrm__Address__c> filterDuplicateAddresses(List<CS_AddressWrapper> addressRecordsBulk, String accountId){
        
        List<cscrm__Address__c> newAddresess = new List<cscrm__Address__c>();
        List<CS_AddressWrapper> addressRecordsBulkCopy = new List<CS_AddressWrapper>(addressRecordsBulk);


        List<String> streetList = new List<String>();
        List<String> cityList = new List<String>();
        List<String> stateList = new List<String>();
        List<String> zipList = new List<String>();
        List<String> countryList = new List<String>();
 
        for(CS_AddressWrapper address : addressRecordsBulk){

            streetList.add(address.street);
            cityList.add(address.city);
            stateList.add(address.state_province);
            zipList.add(address.zipCode);
            countryList.add(address.country);

        }

        List<cscrm__Address__c> existingAddressess = [SELECT Id,cscrm__Building_Number__c,cscrm__Street__c,cscrm__Unit_Number__c,cscrm__City__c,cscrm__State_Province__c,cscrm__Zip_Postal_Code__c,cscrm__Country__c
                                                        FROM cscrm__Address__c 
                                                        WHERE cscrm__Account__c =:accountId AND                                                        
                                                        cscrm__Street__c in :streetList AND
                                                        cscrm__City__c in :cityList AND
                                                        cscrm__State_Province__c in :stateList AND
                                                        cscrm__Zip_Postal_Code__c in :zipList AND
                                                        cscrm__Country__c in :countryList
                                                        ];
        
        if(existingAddressess.isEmpty()){
            newAddresess = CS_AddressDuplicateCheck.createNewAddressess(addressRecordsBulk, accountId);
        }else{
            for(Integer i = (addressRecordsBulk.size()-1) ; i>= 0 ;-- i)
            {
                for(Integer j = 0; j < existingAddressess.size(); j ++)
                {
                    if(existingAddressess[j].cscrm__Street__c == addressRecordsBulk[i].street && existingAddressess[j].cscrm__City__c == addressRecordsBulk[i].city && existingAddressess[j].cscrm__State_Province__c == addressRecordsBulk[i].state_province && existingAddressess[j].cscrm__Zip_Postal_Code__c == addressRecordsBulk[i].zipCode
                        && existingAddressess[j].cscrm__Country__c == addressRecordsBulk[i].country)
                    {
                        addressRecordsBulkCopy.remove(i);
                    }
                }

            }

            newAddresess = CS_AddressDuplicateCheck.createNewAddressess(addressRecordsBulkCopy, accountId);
        }

        return newAddresess;

    }

    public static List<cscrm__Address__c> createNewAddressess(List<CS_AddressWrapper> addressRecordsBulk, String accountId){

        List<cscrm__Address__c> newAddressRecords = new List<cscrm__Address__c>();

        for(CS_AddressWrapper add : addressRecordsBulk){
            cscrm__Address__c address = new cscrm__Address__c();

            address.cscrm__Account__c = accountId;
            address.name = add.name;
            address.cscrm__Street__c = add.street;
            address.cscrm__Street_Name__c = add.streetName;
            address.cscrm__City__c  = add.city;
            address.cscrm__State_Province__c = add.state_province;
            address.cscrm__Zip_Postal_Code__c  = add.zipCode;
            address.cscrm__Country__c  = add.country;
            newAddressRecords.add(address);
        }   

        return newAddressRecords;
    }
}
