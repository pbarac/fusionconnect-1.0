// Added for CLARITY-3654 and CLARITY-3655
    public with sharing class CS_AllCommercialProductTriggerDelegate extends CS_TriggerHandler.DelegateBase {
        public List<cspmb__price_item__c> cspmbList{get; set;}
        public CS_AllCommercialProductTriggerDelegate (List<cspmb__price_item__c> cspmbList){
            
            this.cspmbList = cspmbList;
        }
        List<cspmb__price_item__c> cpList= new List<cspmb__price_item__c>();
        String bandWidth, contractTermValue;
       
        public override void prepareBefore() {
            list<id> cspmbids= new list<id>();
            
            for(cspmb__Price_Item__c cspmb:cspmbList){
                cspmbids.add(cspmb.id);    
            }

            // do any preparation here â€“ bulk loading of data
            if(Trigger.isupdate){
                for(cspmb__Price_Item__c cp: cspmbList ){
                  if(cp.cspmb__Product_Definition_Name__c == 'Access'){
                      if ( cp.fc_down_speed__c >= 1000000) {
                            bandWidth = (cp.fc_down_speed__c/1000000) + ' ' + 'Gb';
                        }else if ( cp.fc_down_speed__c > 1000 ){
                            bandWidth = (cp.fc_down_speed__c/1000) + ' ' + 'Mb';
                        }else {
                            bandWidth = String.valueOf(cp.fc_down_speed__c);
                        }
                        cp.fc_down_speed_str__c = bandWidth;
                    }
                } 
            }
            
            if(Trigger.isUpdate || Trigger.isInsert){
    
                for (cspmb__Price_Item__c cp : cspmbList) {
                    if (cp.cspmb__Contract_Term__c == '1 Month'){
                        contractTermValue = '1MTH';
                    }     
                    else if (cp.cspmb__Contract_Term__c == '12 Months'){
                        contractTermValue = '12MTH';
                    }
                    else if (cp.cspmb__Contract_Term__c == '24 Months'){
                        contractTermValue = '24MTH';
                    }
                    else if (cp.cspmb__Contract_Term__c == '36 Months'){
                        contractTermValue = '36MTH';
                    }
                    else if (cp.cspmb__Contract_Term__c == '48 Months'){
                        contractTermValue = '48MTH';
                    }
                    else if (cp.cspmb__Contract_Term__c == '60 Months'){
                        contractTermValue = '60MTH';
                    }
                    else{
                        contractTermValue = '12MTH';
                    }
    
                    cp.cspmb__Price_Item_Code__c = cp.fc_SKU__c + ' - ' + contractTermValue;
    
                    if (cp.fc_product_family__c == 'Managed Services' && cp.fc_product_group__c == 'SD-WAN (M)' && cp.cspmb__Product_Definition_Name__c == 'SD-WAN') {
                        Boolean containsAdvancedSecurity = cp.name.contains('Advanced Security');
                        Boolean containsHA = cp.name.contains('HA');
                        if (containsAdvancedSecurity && containsHA) {
                            cp.fc_High_Availability__c = 'Yes';
                            cp.fc_Advanced_Security__c = 'Yes';
                        }
                        else if (!containsAdvancedSecurity && containsHA) {
                            cp.fc_High_Availability__c = 'Yes';
                            cp.fc_Advanced_Security__c = 'No';
                        }
                        else if (containsAdvancedSecurity && !containsHA) {
                            cp.fc_High_Availability__c = 'No';
                            cp.fc_Advanced_Security__c = 'Yes';
                        }
                        else if (!containsAdvancedSecurity && !containsHA) {
                            cp.fc_High_Availability__c = 'No';
                            cp.fc_Advanced_Security__c = 'No';
                        }
                    }

                    if (cp.fc_mrr_discountable__c == 'Y') {
                        cp.cspmb__Is_Recurring_Discount_Allowed__c = true;
                    }
                    else{
                        cp.cspmb__Is_Recurring_Discount_Allowed__c = false;
                    }
                    if (cp.fc_nrr_discountable__c == 'Y') {
                        cp.cspmb__Is_One_Off_Discount_Allowed__c = true;
                    }
                    else {
                        cp.cspmb__Is_One_Off_Discount_Allowed__c = false;
                    }
                }            
            }    
        }
    
        public override void prepareAfter() {
    
        }
    
        public override void beforeInsert(SObject o) {
            
        }
    
        public override void beforeUpdate(SObject old, SObject o) {
            // Apply before update logic to this sObject. DO NOT do any SOQL
            // or DML here â€“ store records to be modified in an instance variable
            // which can be processed by the finish() method
        }
    
        public override void beforeDelete(SObject o) {
            // Apply before delete logic to this sObject. DO NOT do any SOQL
            // or DML here â€“ store records to be modified in an instance variable
            // which can be processed by the finish() method
        }
    
        public override void afterInsert(SObject o) {
            // Apply after insert logic to this sObject. DO NOT do any SOQL
            // or DML here â€“ store records to be modified in an instance variable
            // which can be processed by the finish() method
        }
    
        public override void afterUpdate(SObject old, SObject o) {
        
        }
    
        public override void afterDelete(SObject o) {
            // Apply after delete logic to this sObject. DO NOT do any SOQL
            // or DML here â€“ store records to be modified in an instance variable
            // which can be processed by the finish() method
        }
    
        public override void afterUndelete(SObject o) {
            // Apply after undelete logic to this sObject. DO NOT do any SOQL
            // or DML here â€“ store records to be modified in an instance variable
            // which can be processed by the finish() method
        }
    
        public override void finishBefore() {
    
        }
    
        public override void finishAfter() {

        }
      
    }