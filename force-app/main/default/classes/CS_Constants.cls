public class CS_Constants {
       
    public static final String ORG_ID = 'f754cb6e-4141-43f7-b2c4-d3227946802c';
    public static final String CLIENT_ID = '033248d7-8a4f-4846-9ef2-a4ca00d1317b';
    //public static final String CLIENT_SECRET = '3df3de9b27a5e5a6480323cf0893a36a9047fad0b24392be809ca06929d7a2';
    public static final String CLIENT_SECRET = '3df3de9b27a5e5a6480323cf0893a36a9047fad0b24392be809ca06929d7a2b7';
    public static final String FULL_ENDPOINT = 'https://cs-pre-sandbox-eu.herokuapp.com';
    public static final String No = 'No';
    public static final String Yes = 'Yes';

    public static final String REQUIRED_ATTRIBUTES_CS_AccessCarrierBandwidthLookup = '["IP Type Value", "Tech Type Value", "Network Type Value", "Access Carrier Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_AccessCarrierLookup = '["IP Type Value", "Tech Type Value", "Network Type Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_AccessIpTypeLookup = '["id","name"]';
    public static final String REQUIRED_ATTRIBUTES_CS_AccessTechnologyTypLookup = '["Id","Name"]';
    public static final String REQUIRED_ATTRIBUTES_CS_AccessTechTypeLookup = '["IP Type Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_DeviceAddonsCPETypeLookup = '["Id","Name"]';
    public static final String REQUIRED_ATTRIBUTES_CS_NetworkTypeLookup = '["IP Type Value", "Tech Type Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_SDWANAdvSecurityLookup = '["Id", "Name", "SDWAN Type Value", "Prioritized bandwidth Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_SDWANHardwareModelLookup = '["Id", "Name", "Hardware Manufacturer Value", "SDWAN Type Value", "Advanced Security Value", "Prioritized bandwidth Value", "High Availability Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_SDWANHighAvailLookup = '["Id", "Name", "SDWAN Type Value", "Prioritized bandwidth Value", "Advanced Security Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_SDWANOEAddressLookup = '["Id", "Name", "basketId"]';
    public static final String REQUIRED_ATTRIBUTES_CS_SDWANPrioritizedBandwidthLookup = '["Id", "Name", "SDWAN Type Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_SDWANTechnologyTypLookup = '["Id","Name"]';
    public static final String REQUIRED_ATTRIBUTES_CS_SDWANTechTypeLookup = '["Vendor Name Value"]';
    public static final String REQUIRED_ATTRIBUTES_CS_SDWANVendorLookup = '["Id", "Name", "SDWAN Type Value", "Prioritized bandwidth Value", "Advanced Security Value", "High Availability Value"]';

    public static final String Gb = 'Gb';
    public static final String Mb = 'Mb';
    public static final String Month1 = '1 Month';
    public static final String MTH1 = '1MTH';
    public static final String Month12 = '12 Months';
    public static final String MTH12 = '12MTH';
    public static final String Month24 = '24 Months';
    public static final String MTH24 = '24MTH';
    public static final String Month36 = '36 Months';
    public static final String MTH36 = '36MTH';
    public static final String Month48 = '48 Months';
    public static final String MTH48 = '48MTH';
    public static final String Month60 = '60 Months';
    public static final String MTH60 = '60MTH';

    public static final String Product_Family_ManagedServices = 'Managed Services';
    public static final String Product_Group_SDWANM = 'SD-WAN (M)';
    public static final String Product_Definition_SDWAN = 'SD-WAN';
    public static final String Product_Definition_SDWAN_Add_On = 'SD-WAN Add On';
    public static final String Product_Definition_Access = 'Access';
    public static final String Product_Definition_Access_Add_On = 'Access Add On';

    public static final String AdvancedSecurity = 'Advanced Security';
    public static final String HA = 'HA';

    public static final String SE_Approval_Status_In_Progress = 'In Progress';
    public static final String SE_Approval_Status_Approval_Needed = 'Approval Needed';
    public static final String Product_Basket_Status_In_Progress = 'In Progress';
    public static final String Product_Basket_Status_Valid = 'Valid';

    public static final String CSPOFA_Status_Complete = 'Complete';
    public static final String CSPOFA_Status_Error = 'Error';

    public static final String Pricebook_Type_Catalogue = 'Pricebook (catalogue)';

    public static final String Add_On_Type_Access = 'ACCESS ADDON';
    public static final String Add_On_Type_SDWAN = 'SDWAN ADDON';
    public static final String Add_On_Type_NA_Access_Core = 'NA ACCESS CORE';
    public static final String Add_On_Type_NA_SDWAN_Core = 'NA SDWAN CORE';

    public static final String Price_Item_Role_Master = 'Master';
    public static final String Price_Item_Role_Variant = 'Variant';
    public static final String Price_Item_Type_Commercial_Product = 'Commercial Product';

    public static final String Material_Type_Technical_SKU = 'Technical SKU';
    public static final String Related_Product_ContractTerm = 'contractTerm';
    public static final String Related_Product_cpID = 'cpID';
    public static final String CP_Professional_Install = 'Professional On-Site Install';
}