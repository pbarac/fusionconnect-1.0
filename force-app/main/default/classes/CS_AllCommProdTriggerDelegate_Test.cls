@isTest
public class CS_AllCommProdTriggerDelegate_Test {

    @TestSetup
    static void CreateTestData(){
        cspmb__Price_Item__c PriceItemSDWAN = CS_TestDataFactory.createPriceItem(false);
        PriceItemSDWAN.name = 'TestPriceItemSDWAN';
        PriceItemSDWAN.cspmb__Product_Definition_Name__c = 'SD-WAN';
        PriceItemSDWAN.fc_product_family__c = 'Managed Services';
        PriceItemSDWAN.fc_product_group__c = 'SD-WAN (M)';
        PriceItemSDWAN.fc_SKU__c = 'Test';
        insert PriceItemSDWAN;
    }

    @isTest
    public static void TriggerDelegateTestMethod(){
            cspmb__Price_Item__c Acp = CS_TestDataFactory.createPriceItem(false);{
            Acp.Name='Test1';
            Acp.cspmb__Type__c ='Commercial Product';
            Acp.cspmb__Role__c ='Master';
            Acp.cspmb__Product_Definition_Name__c ='Access';    
            Acp.fc_down_speed__c =3000;
                
            insert Acp;
           }
       
            test.startTest();
            Acp.fc_down_speed__c =5000;
            update Acp;
            test.stopTest();
            cspmb__Price_Item__c test= [select id,fc_down_speed__c,fc_down_speed_str__c
                                    from cspmb__Price_Item__c
                                    where fc_down_speed__c > 0
                                    and cspmb__Product_Definition_Name__c = 'Access'];
            system.assertEquals(5000, test.fc_down_speed__c);
            system.assertEquals('5 Mb', test.fc_down_speed_str__c);

            Acp.fc_down_speed__c =5000000;
            update Acp;
            test = [select id,fc_down_speed__c,fc_down_speed_str__c
                                    from cspmb__Price_Item__c
                                    where fc_down_speed__c > 0
                                    and cspmb__Product_Definition_Name__c = 'Access'];
            system.assertEquals(5000000, test.fc_down_speed__c);
            system.assertEquals('5 Gb', test.fc_down_speed_str__c);

            Acp.fc_down_speed__c =500;
            update Acp;
            test = [select id,fc_down_speed__c,fc_down_speed_str__c
                                    from cspmb__Price_Item__c
                                    where fc_down_speed__c > 0
                                    and cspmb__Product_Definition_Name__c = 'Access'];
            system.assertEquals(500, test.fc_down_speed__c);
            system.assertEquals('500', test.fc_down_speed_str__c);
        }
   
    @isTest
    public static void TestContractTermValue() {
        cspmb__price_item__c PriceItemSDWANTest = [select id,name,cspmb__Contract_Term__c,cspmb__Price_Item_Code__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItemSDWAN'];

        PriceItemSDWANTest.cspmb__Contract_Term__c = '1 Month';

        update PriceItemSDWANTest;

        cspmb__price_item__c PriceItemSDWANTest1MTH = [select id,name,cspmb__Contract_Term__c,cspmb__Price_Item_Code__c, fc_SKU__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItemSDWAN'];
        System.assertEquals('Test - 1MTH', PriceItemSDWANTest1MTH.cspmb__Price_Item_Code__c, '1 month contract term price item failed');
        
        PriceItemSDWANTest.cspmb__Contract_Term__c = '12 Months';
        
        update PriceItemSDWANTest;

        cspmb__price_item__c PriceItemSDWANTest12MTH = [select id,name,cspmb__Contract_Term__c,cspmb__Price_Item_Code__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItemSDWAN'];
        System.assert(PriceItemSDWANTest12MTH.cspmb__Price_Item_Code__c.contains('12MTH'));
        
        PriceItemSDWANTest.cspmb__Contract_Term__c = '24 Months';
        
        update PriceItemSDWANTest;

        cspmb__price_item__c PriceItemSDWANTest24MTH = [select id,name,cspmb__Contract_Term__c,cspmb__Price_Item_Code__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItemSDWAN'];
        System.assert(PriceItemSDWANTest24MTH.cspmb__Price_Item_Code__c.contains('24MTH'));
        
        PriceItemSDWANTest.cspmb__Contract_Term__c = '36 Months';
        
        update PriceItemSDWANTest;

        cspmb__price_item__c PriceItemSDWANTest36MTH = [select id,name,cspmb__Contract_Term__c,cspmb__Price_Item_Code__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItemSDWAN'];
        System.assert(PriceItemSDWANTest36MTH.cspmb__Price_Item_Code__c.contains('36MTH'));

        PriceItemSDWANTest.cspmb__Contract_Term__c = '48 Months';
        
        update PriceItemSDWANTest;

        cspmb__price_item__c PriceItemSDWANTest48MTH = [select id,name,cspmb__Contract_Term__c,cspmb__Price_Item_Code__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItemSDWAN'];
        System.assert(PriceItemSDWANTest48MTH.cspmb__Price_Item_Code__c.contains('48MTH'));

        PriceItemSDWANTest.cspmb__Contract_Term__c = '60 Months';
        
        update PriceItemSDWANTest;

        cspmb__price_item__c PriceItemSDWANTest60MTH = [select id,name,cspmb__Contract_Term__c,cspmb__Price_Item_Code__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItemSDWAN'];
        System.assert(PriceItemSDWANTest60MTH.cspmb__Price_Item_Code__c.contains('60MTH'));
    }

    @isTest
    public static void TestDiscountAllowedValues () {
        cspmb__price_item__c PriceItemSDWANTest = [select id,name,fc_mrr_discountable__c,cspmb__Is_Recurring_Discount_Allowed__c,fc_nrr_discountable__c,cspmb__Is_One_Off_Discount_Allowed__c
        from cspmb__Price_Item__c
        where name =: 'TestPriceItemSDWAN'];

        PriceItemSDWANTest.fc_mrr_discountable__c = 'Y';
        PriceItemSDWANTest.fc_nrr_discountable__c = 'Y';

        update PriceItemSDWANTest;

        cspmb__price_item__c PriceItemSDWANTestYY = [select id,name,fc_mrr_discountable__c,cspmb__Is_Recurring_Discount_Allowed__c,fc_nrr_discountable__c,cspmb__Is_One_Off_Discount_Allowed__c
        from cspmb__Price_Item__c
        where name =: 'TestPriceItemSDWAN'];

        System.assert(PriceItemSDWANTestYY.cspmb__Is_Recurring_Discount_Allowed__c == true);
        System.assert(PriceItemSDWANTestYY.cspmb__Is_One_Off_Discount_Allowed__c == true);

        PriceItemSDWANTestYY.fc_mrr_discountable__c = 'N';
        PriceItemSDWANTestYY.fc_nrr_discountable__c = 'N';

        update PriceItemSDWANTestYY;

        cspmb__price_item__c PriceItemSDWANTestNN = [select id,name,fc_mrr_discountable__c,cspmb__Is_Recurring_Discount_Allowed__c,fc_nrr_discountable__c,cspmb__Is_One_Off_Discount_Allowed__c
        from cspmb__Price_Item__c
        where name =: 'TestPriceItemSDWAN'];

        System.assert(PriceItemSDWANTestNN.cspmb__Is_Recurring_Discount_Allowed__c == false);
        System.assert(PriceItemSDWANTestNN.cspmb__Is_One_Off_Discount_Allowed__c == false);

    }

    @isTest
    public static void TestHighAvailabilityAndAdvancedSecurityValues () {

        cspmb__price_item__c PriceItemSDWANTest = [select id,name,fc_High_Availability__c,fc_Advanced_Security__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItemSDWAN'];
        System.assert(PriceItemSDWANTest.fc_High_Availability__c == 'No');
        System.assert(PriceItemSDWANTest.fc_Advanced_Security__c == 'No');

        PriceItemSDWANTest.name = 'TestPriceItem - HA';
        
        update PriceItemSDWANTest;
        
        cspmb__price_item__c PriceItemSDWANTestHA = [select id,name,fc_High_Availability__c,fc_Advanced_Security__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItem - HA'];
        System.assert(PriceItemSDWANTestHA.fc_High_Availability__c == 'Yes');
        System.assert(PriceItemSDWANTestHA.fc_Advanced_Security__c == 'No');
        
        PriceItemSDWANTest.name = 'TestPriceItem - Advanced Security - HA';
        
        update PriceItemSDWANTest;
        
        cspmb__price_item__c PriceItemSDWANTestAdvancedSecurityHA = [select id,name,fc_High_Availability__c,fc_Advanced_Security__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItem - Advanced Security - HA'];
        System.assert(PriceItemSDWANTestAdvancedSecurityHA.fc_High_Availability__c == 'Yes');
        System.assert(PriceItemSDWANTestAdvancedSecurityHA.fc_Advanced_Security__c == 'Yes');


        PriceItemSDWANTest.name = 'TestPriceItem - Advanced Security';
        
        update PriceItemSDWANTest;
        
        cspmb__price_item__c PriceItemSDWANTestAdvancedSecurity = [select id,name,fc_High_Availability__c,fc_Advanced_Security__c
                                                    from cspmb__Price_Item__c
                                                    where name =: 'TestPriceItem - Advanced Security'];
        System.assert(PriceItemSDWANTestAdvancedSecurity.fc_High_Availability__c == 'No');
        System.assert(PriceItemSDWANTestAdvancedSecurity.fc_Advanced_Security__c == 'Yes');


    }
        
}