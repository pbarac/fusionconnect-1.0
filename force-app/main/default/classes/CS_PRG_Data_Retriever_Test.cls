@isTest public with sharing class CS_PRG_Data_Retriever_Test {
    @isTest public static void CS_PRG_Data_Retriever_Test() {

        Test.startTest();

        Map<String, Object> data  = CS_PRG_Data_Retriever.getData(null);

        Test.stopTest();

        System.assertNotEquals(data, null, 'Data not fetched');
    }
}