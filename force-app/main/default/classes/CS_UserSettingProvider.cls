/**
 * Created by Fayaz Shaik
 */

global class CS_UserSettingProvider implements cssmgnt.RemoteActionDataProvider {
	global Map<String, Object> getData(Map<String, Object> inputMap) {
		Map<String, Object> retVal = new Map<String, Object> {
			'currencySymbol' => getCurrencySymbolFromIso(UserInfo.getDefaultCurrency())
		};

		return retVal;
	}

	private static String getCurrencySymbolFromIso(String iso) {
		switch on iso {
			when 'USD', 'CAD', 'SGD', 'AUD', 'NZD' {
				return '$';
			}
			when 'EUR' {
				return '€';
			}
			when 'GBP' {
				return '£';
			}
			when else {
				return iso;
			}
		}
	}
}