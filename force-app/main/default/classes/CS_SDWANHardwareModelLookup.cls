/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANHardwareModelLookup - Lookup for Hardware model in solution management 
*/
global class CS_SDWANHardwareModelLookup extends cscfga.ALookupSearch {

    public static final String REQUIRED_ATTRIBUTES = CS_Constants.REQUIRED_ATTRIBUTES_CS_SDWANHardwareModelLookup;

    // Attribute names used in the WHERE clause of SOQL query, JSON notation, e.g. '["Attribute1","Attribute2"]'    
    public override String getRequiredAttributes() {
        return REQUIRED_ATTRIBUTES;
    }    
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){

        List<cspmb__Price_Item__c> commProdLst = new List<cspmb__Price_Item__c>();
        String vendorNameValue = searchFields.get('Hardware Manufacturer Value');
        String techTypeValue = searchFields.get('SDWAN Type Value');
        String pbValue = searchFields.get('Prioritized bandwidth Value');
        String advSecValue = searchFields.get('Advanced Security Value');
        String highAvlValue = searchFields.get('High Availability Value');      
        //system.debug(vendorNameValue + techTypeValue + pbValue + advSecValue + highAvlValue);         

        
        List<AggregateResult> agr = [SELECT min(Id) cpid, fc_default_cpe__c FROM cspmb__Price_Item__c 
                                      WHERE cspmb__Product_Definition_Name__c = :CS_Constants.Product_Definition_SDWAN and fc_vendor__c =: vendorNameValue and fc_tech_type__c =: techTypeValue and
                                       fc_prioritized_bandwidth__c =: pbValue and fc_Advanced_Security__c =: advSecValue and fc_High_Availability__c =: highAvlValue and fc_default_cpe__c != null
                                        group by fc_default_cpe__c order by fc_default_cpe__c];
        
        for (AggregateResult ar : agr){
                cspmb__Price_Item__c cp = new cspmb__Price_Item__c(Id = string.Valueof(ar.get('cpid')), Name = string.Valueof(ar.get('fc_default_cpe__c')), fc_hardware_model__c = string.Valueof(ar.get('fc_default_cpe__c')));
                commProdLst.add(cp);
        }

        return commProdLst;
    }

}