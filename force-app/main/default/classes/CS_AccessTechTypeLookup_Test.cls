@isTest
private class CS_AccessTechTypeLookup_Test {
    
    @isTest static void testDoLookupSearch() {
        
        //create a commercial product
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_IP_type__c = 'Static';
        cpi.fc_tech_type__c = 'WiFi';
        insert cpi;         

        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('IP Type Value', 'Static');

        Test.startTest();
        CS_AccessTechTypeLookup testObject = new CS_AccessTechTypeLookup();
        testObject.getRequiredAttributes();
        Object[] techTypeCPLst = testObject.doLookupSearch(searchFields, null, null, null, null);
        Test.stopTest();

        System.assertNotEquals(techTypeCPLst.size(), null, 'Tech types are not retrieved');
        cpi = (cspmb__Price_Item__c) techTypeCPLst[0];
        System.assertEquals(cpi.name, 'WiFi', 'Tech type is not correct');
    }
}