global with sharing class CS_PRG_Data_Retriever implements cssmgnt.RemoteActionDataProvider {

    @RemoteAction
    global static Map<String, Object> getData(Map<String, Object> inputMap) {
      Map<String, Object> prToPrg = new Map<String, Object>();
        
      List<cspmb__Price_Item_Pricing_Rule_Association__c> cppras = [
          SELECT Id, Name,cspmb__pricing_rule__c,
              cspmb__pricing_rule__r.cspmb__pricing_rule_code__c,
              cspmb__pricing_rule__r.cspmb__description__c,
              cspmb__pricing_rule__r.name
          FROM cspmb__Price_Item_Pricing_Rule_Association__c
          where cspmb__pricing_rule__r.name in ('Standard Pricing Rule')
      ];
      for (cspmb__Price_Item_Pricing_Rule_Association__c cppra : cppras) {
        prToPrg.put(
            cppra.Name,
            JSON.serialize(new PricingRule(cppra))
        );
      }
      return prToPrg;
    }
  
    private class PricingRule {
      private String name;
      private String description;
  
      private PricingRule(cspmb__Price_Item_Pricing_Rule_Association__c cppra) {
        this.name = cppra.cspmb__pricing_rule__r.name;
        this.description = cppra.cspmb__pricing_rule__r.cspmb__description__c;
      }
    }
  }