/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANHighAvailLookup_Test - Test coverage for class CS_SDWANHighAvailLookup
*/
@isTest 
public class CS_SDWANHighAvailLookup_Test {

    @isTest public static void doLookupSearchTest(){

        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'SD-WAN';
        insert cpi; 
      
        Test.startTest();
        Map<String,String> searchFields = new Map <String, String>();
        searchFields.put('Vendor Name Value', 'Meraki');
        searchFields.put('SDWAN Type Value', 'WiFi');
        searchFields.put('Prioritized bandwidth Value', '100 Mbps');
        searchFields.put('Advanced Security Value', 'No');
                
        CS_SDWANHighAvailLookup testObject = new CS_SDWANHighAvailLookup();
        testObject.getRequiredAttributes();
        Object[] HighAvailList = testObject.doLookupSearch(searchFields,null,null,null,null);
        Test.stopTest();
        
        System.assertNotEquals(HighAvailList.size(), null, 'High Availability Value not fetched');
    }
}