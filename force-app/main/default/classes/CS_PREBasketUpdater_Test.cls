@isTest public with sharing class CS_PREBasketUpdater_Test {
    @isTest public static void CS_PREBasketUpdater_Test() {

        Account acc = CS_TestDataFactory.createAccount(true);
        List<cscfga__Product_Basket__c> pbLst = CS_TestDataFactory.createBaskets(1, acc.Id, true);
        
        Map<String, Object> inputMap = new Map<String, Object>{
            pbLst[0].Id => 'test'
        };

        Test.startTest();
        Map<String, Object> data  = CS_PREBasketUpdater.getData(null);

        data  = CS_PREBasketUpdater.getData(inputMap);

        Test.stopTest();

        System.assertNotEquals(data, null, 'Data not fetched');
    }
}