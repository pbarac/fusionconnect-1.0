/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANTechnologyTypLookup_Test - Test coverage for class CS_SDWANTechnologyTypLookup
*/
@isTest 
public class CS_SDWANTechnologyTypLookup_Test {

    @isTest public static void doLookupSearchTest(){
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'SD-WAN';
        insert cpi; 
        
        Test.startTest();
        
        Map<String,String> searchFields = new Map <String, String>();
        searchFields.put('Vendor Name Value', 'Meraki');
        
        CS_SDWANTechnologyTypLookup testObject = new CS_SDWANTechnologyTypLookup();
        testObject.getRequiredAttributes(); 
        Object[] TechList = testObject.doLookupSearch(searchFields,null,null,null,null);
        Test.stopTest();
        
        System.assertNotEquals(TechList.size(), null, 'Tech Type Value not fetched');
    }
}