@isTest
public with sharing class CS_AddressValidationController_Test {
    static testMethod void test_getAddressRecords(){

        Account acc = CS_TestDataFactory.createAccount(true);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.createBaskets(1, acc.id, true)[0];

        cscrm__Address__c address = CS_TestDataFactory.createAddress(false);
        address.cscrm__Account__c = acc.id;
        insert address; 

        Test.startTest();
        String result = CS_AddressValidationController.getAddressRecords(basket.id);
        System.assertNotEquals(null, result);
        
        Test.stopTest();
    }

    static testMethod void test_getFieldsFromFieldSet(){

        String objectApiName = 'cscrm__Address__c';
        String fieldSetName = 'addAddressFS';
        Test.startTest();
            String result = CS_AddressValidationController.getFieldsFromFieldSet(objectApiName,fieldSetName );
            System.assertNotEquals(null, result);
        Test.stopTest();

    }

    static testMethod void test_saveAddressRecords(){

        Account acc = CS_TestDataFactory.createAccount(true);

        CS_AddressWrapper addWrapper = new CS_AddressWrapper();
        addWrapper.street ='Test street';
        addWrapper.city = 'Test city';
        addWrapper.state_province = 'Test state_province';
        addWrapper.zipCode = 'Test zip code';
        addWrapper.country = 'Test country';
        addWrapper.name = 'Test name';

        List<CS_AddressWrapper> addWrapperList = new List<CS_AddressWrapper>();
        Test.startTest();
        String resultData = CS_AddressValidationController.saveAddressRecords(JSON.serialize(addWrapperList), acc.id);
        System.assertNotEquals(null, resultData);
        Test.stopTest();
    }
}


