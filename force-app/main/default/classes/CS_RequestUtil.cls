public with sharing class CS_RequestUtil {

	public static HttpResponse sendRequest(String method, String endpoint, String payload) {
		HttpRequest request = new HttpRequest();

		// Setting headers using shared secret strategy
		CS_SharedSecretSignService svc = new CS_SharedSecretSignService();
		request = svc.getHeadersSharedSecret(request, method, endpoint, payload);

		// Setting header using rsa signing strategy
		// RSASignService rsasvc = new RSASignService();
		// request = rsasvc.getHeadersRsa(request, method, endpoint, payload);

		request.setMethod(method);
		request.setEndpoint(CS_Constants.FULL_ENDPOINT + endpoint);
		if (payload != null && payload != '') {
			request.setBody(payload);
		}

		Http http = new Http();
        system.debug('before request');
		system.debug(request);     
        system.debug(request.getHeader('organization'));
		HttpResponse response = http.send(request);

		return response;
	}
}