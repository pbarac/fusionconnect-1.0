global class CS_AccessCarrierLookup extends cscfga.ALookupSearch {

    public static final String REQUIRED_ATTRIBUTES = CS_Constants.REQUIRED_ATTRIBUTES_CS_AccessCarrierLookup;

    // Attribute names used in the WHERE clause of SOQL query, JSON notation, e.g. '["Attribute1","Attribute2"]'
    public override String getRequiredAttributes() {
        return REQUIRED_ATTRIBUTES;
    }    
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){

        List<cspmb__Price_Item__c> commProdLst = new List<cspmb__Price_Item__c>();
        String ipTypeValue = searchFields.get('IP Type Value');
        String techTypeValue = searchFields.get('Tech Type Value');
        String networkType = searchFields.get('Network Type Value');
        system.debug(searchFields);
        system.debug(ipTypeValue + techTypeValue + networkType );
        List<AggregateResult> agr = [SELECT min(Id) cpid, fc_vendor__c FROM cspmb__Price_Item__c 
                                      WHERE cspmb__Product_Definition_Name__c =: CS_Constants.Product_Definition_Access and fc_IP_type__c =: ipTypeValue and fc_tech_type__c =: techTypeValue and fc_on_network__c =: networkType and fc_vendor__c != null
                                      group by fc_vendor__c order by fc_vendor__c];
        
        for (AggregateResult ar : agr){
                cspmb__Price_Item__c cp = new cspmb__Price_Item__c(id = string.Valueof(ar.get('cpid')), Name = string.Valueof(ar.get('fc_vendor__c')));
                commProdLst.add(cp);
        }

        return commProdLst;
    }

}