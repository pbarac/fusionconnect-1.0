global with sharing class CS_PREConfigurationUpdater implements cssmgnt.RemoteActionDataProvider {

	abstract class Common {
		public String version;
		public String id;

		public Common() {
			this.version = '3-0-0';
		}
	}

	virtual class CommonCart extends Common {
		public List<String> pricingRuleGroupCodes;
		public List<CartItem> items;

		public CommonCart() {}

		public CommonCart(List<String> pricingRuleGroupCodes) {
			super();
			this.id = uuidv4();
			this.pricingRuleGroupCodes = pricingRuleGroupCodes;
			this.items = new List<CartItem>();
		}
	}

	virtual class CartItem extends Common {
		public Integer quantity;
		public String catalogueItemId;

		public CartItem() {}

		public CartItem(String id, String catalogueItemId) {
			super();
			this.id = id;
			this.quantity = 1; // quantity is always 1 for solution console
			this.catalogueItemId = catalogueItemId;
		}
	}

	class Response {
		public List<String> actions;
		public ResponseCart cart;

		public Response() {}
	}

	class ResponseCart extends CommonCart {
		public List<ResponseCartItem> items;
	}

	class ResponseCartItem extends CartItem {
		public Pricing pricing;
	}

	class Pricing {
		public List<Discount> discounts;
	}

	class Discount {
		public String version;
		public String discountCharge;
		public String chargeType;
		public String discountPrice;
		public String type;
		public Double amount;
		public String description;
		public Integer duration;
		public Integer recurringOffset;
		public String source;
		public String evaluationOrder;
		public String recordType;
		public List<Discount> memberDiscounts;
		// TODO: missing customData property
	}

	private static String uuidv4() {
		Blob b = Crypto.GenerateAESKey(128);
		String h = EncodingUtil.ConvertTohex(b);
		String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);

		return guid;
	}

	private static List<cscfga__Product_Configuration__c> getBasketConfigurations(String basketId) {
		List<cscfga__Product_Configuration__c> configurations = [
			SELECT Id,
				cscfga__discounts__c,
			(
				SELECT Name, cscfga__Value__c
				FROM cscfga__Attributes__r
				WHERE Name IN ('CommercialProductCode', 'PRE Discounts', 'GUID')
			)
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :basketId
		];

		return configurations;
	}

	private static List<String> getBasketPRGs(String basketId) {
		return CS_Manage_PRGs_Controller.getSelectedPRGs(basketId);
	}

	public static CommonCart createCommonCart(String basketId) {
		List<cscfga__Product_Configuration__c> configurations = getBasketConfigurations(basketId);
		List<String> prgs = getBasketPRGs(basketId);

		CommonCart cart = new CommonCart(prgs);
		for (cscfga__Product_Configuration__c c : configurations) {
			String catalogueItemId;
			String id;

			for (cscfga__Attribute__c attr : c.cscfga__Attributes__r) {
				if (attr.Name == 'CommercialProductCode') {
					catalogueItemId = attr.cscfga__Value__c;
				} else if (attr.Name == 'GUID') {
					id = attr.cscfga__Value__c;
				}
			}

			if (catalogueItemId != null && id != null) {
				CartItem ci = new CartItem(
					id, // config GUID attribute value
					catalogueItemId// config CommercialProductCode attribute value
				);

				cart.items.add(ci);
			}
		}

		return cart;
	}

	public static void recalculateBasketDiscounts(String basketId) {
		System.debug('PREConfigurationUpdater:recalculateBasketDiscounts');

		CommonCart cart = createCommonCart(basketId);

		Map<String, Object> rawResponse = CS_PREApiV6Pricings.getData(
			new Map<String, Object> {
				'payload' => JSON.serialize(cart)
			}
		);

		System.debug('Raw response:');
		System.debug(rawResponse);

		System.debug('Response:');
		Response response = (Response) JSON.deserialize((String)rawResponse.get('body'), Response.class);
		System.debug(response);

		updateConfigurationDiscounts(basketId, response);
	}

	private static void updateConfigurationDiscounts(String basketId, Response response) {
		System.debug('PREConfigurationUpdater:updateConfigurationDiscounts');

		Map<String, List<Discount>> guidToDiscountsMap = new Map<String, List<Discount>>();
		for (ResponseCartItem ci : response.cart.items) {
			guidToDiscountsMap.put(ci.id, ci.pricing.discounts);
		}

		List<cscfga__Attribute__c> attrs = [
			SELECT cscfga__Display_Value__c, cscfga__Product_Configuration__r.cscfga__discounts__c
			FROM cscfga__Attribute__c
			WHERE cscfga__Display_Value__c IN :guidToDiscountsMap.keySet()
		];

		List<cscfga__Product_Configuration__c> configsForUpdate = new List<cscfga__Product_Configuration__c>();
		for (cscfga__Attribute__c attr : attrs) {
			String guid = attr.cscfga__Display_Value__c;
			List<Discount> discountsFromResponse = guidToDiscountsMap.get(guid);

			if (discountsFromResponse != null && discountsFromResponse.size() > 0) {
				attr.cscfga__Product_Configuration__r.cscfga__discounts__c = '{"discounts":' + JSON.serialize(discountsFromResponse) +'}';
				System.debug('Adding discount json ' + attr.cscfga__Product_Configuration__r.cscfga__discounts__c);

				configsForUpdate.add(attr.cscfga__Product_Configuration__r);
			}
		}

		if (configsForUpdate != null && configsForUpdate.size() > 0) {
			System.debug('Updating ' + configsForUpdate.size() + ' configurations.');
			update configsForUpdate;

			Set<Id> basketIds = new Set<Id>();
			basketIds.add(basketId);
			cscfga.ProductConfigurationBulkActions.calculateTotals(basketIds);		}
	}

	/**
	 * In order to be invoked from the Solution Console it implements the cssmgnt.RemoteActionDataProvider
	 * @param inputMap
	 * @example
	 * {
	 *	 [configurationId]: '{"discounts": <discount structure goes here> }'
	 * }
	 */
	@RemoteAction
	global static Map<String, Object> getData(Map<String, Object> inputMap) {
		try {
			Set<String> configurationIds = inputMap.keySet();

			Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>([
				SELECT Id, cscfga__discounts__c, cscfga__product_basket__c
				FROM cscfga__Product_Configuration__c
				WHERE Id in :configurationIds
			]);

			List<cscfga__Product_Configuration__c> configsToUpdate = new List<cscfga__Product_Configuration__c>();
			for (cscfga__Product_Configuration__c c : configs.values()) {
				Object payloadObj = inputMap.get(c.Id);
				if (payloadObj != null) {
					String payloadString = payloadObj.toString();
					Map<String, Object> payload = (Map<String, Object>) JSON.deserializeUntyped(payloadString);

					c.cscfga__discounts__c = '{"discounts":' + JSON.serialize(payload.get('discounts')) +'}';
					System.debug(c.cscfga__discounts__c);

					configsToUpdate.add(c);
				}
			}

			update configsToUpdate;

			Set<Id> basketIds = new Set<Id>();
			basketIds.add(configs.values().get(0).cscfga__Product_Basket__c);
			cscfga.ProductConfigurationBulkActions.calculateTotals(basketIds);

			return new Map<String, Object>{
				'status' => 'ok'
			};
		} catch (Exception e) {
			return new Map<String, Object>{
				'status' => 'error',
				'message' => e.getMessage()
			};
		}
	}
}