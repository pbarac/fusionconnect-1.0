/**
* @description CS_DeviceAddonsCPETypeLookup_Test - Test coverage for class CS_DeviceAddonsCPETypeLookup
*/
@isTest 
public class CS_DeviceAddonsCPETypeLookup_Test {

    @isTest public static void doLookupSearchTest(){
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_cpe_type__c = 'Router';
        insert cpi; 
        
        Test.startTest();
        
        Map<String,String> searchFields = new Map <String, String>();
        searchFields.put('CPE Type', 'Router');
        
        CS_DeviceAddonsCPETypeLookup testObject = new CS_DeviceAddonsCPETypeLookup();
        testObject.getRequiredAttributes(); 
        Object[] CPEList = testObject.doLookupSearch(searchFields,null,null,null,null);
        Test.stopTest();
        
        System.assertNotEquals(CPEList.size(), null, 'CPE Type Value not fetched');
    }
}