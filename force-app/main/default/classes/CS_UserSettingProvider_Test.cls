@isTest public with sharing class CS_UserSettingProvider_Test {
    @isTest public static void CS_UserSettingProvider_Test() {

        
        Test.startTest();
        
        CS_UserSettingProvider testObject = new CS_UserSettingProvider();
        Map<String, Object> retInputMap = testObject.getData(null); 
        String symbol = CS_UserSettingProvider.getCurrencySymbolFromIso('USD');
        String symbol2 = CS_UserSettingProvider.getCurrencySymbolFromIso('EUR');
        String symbol3 = CS_UserSettingProvider.getCurrencySymbolFromIso('GBP');
        String symbol4 = CS_UserSettingProvider.getCurrencySymbolFromIso('ABC');
        Test.stopTest();
        
        System.assertNotEquals(symbol, null, 'Symbol not fetched');
        System.assertNotEquals(symbol2, null, 'Symbol not fetched');
        System.assertNotEquals(symbol3, null, 'Symbol not fetched');
        System.assertNotEquals(symbol4, null, 'Symbol not fetched');
    }
}