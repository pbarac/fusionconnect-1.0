global class CS_CommercialProductPRELookup implements cssmgnt.RemoteActionDataProvider {
	global Map<String, Object> getData(Map<String, Object> inputMap) {
		String sku = inputMap.get('sku') != null ? String.valueOf(inputMap.get('sku')) : '';

		system.debug('sku'+ sku);
		
        List<cspmb__Price_Item__c> priceItems = [
			select Id, Name, cspmb__Product_Definition_Name__c
			from cspmb__Price_Item__c
			where cspmb__Price_Item_Code__c = :sku
		];

        
        system.debug('priceItems'+ [
			select Id, Name, cspmb__Product_Definition_Name__c,cspmb__Price_Item_Code__c
			from cspmb__Price_Item__c
			//where cspmb__Price_Item_Code__c = :sku
		]);
		cspmb__Price_Item__c priceItem = null;
		if (!priceItems.isEmpty()) {
			priceItem = priceItems[0];
		}

		String componentName = null;
		List<cscfga__Product_Definition__c> pds = [
			select id
			from cscfga__Product_Definition__c
			where name = :priceItem.cspmb__Product_Definition_Name__c
		];

		if (!pds.isEmpty()) {
			List<cssdm__Solution_Definition__c> sds = [
				select Name
				from cssdm__Solution_Definition__c
				where cssdm__product_definition__c = :pds[0].id
			];

			if (!sds.isEmpty()) {
				componentName = sds[0].Name;
			}
		}

		Map<String, Object> retVal = new Map<String, Object> {
			'commercialProduct' => priceItem,
			'componentName' => componentName
		};

		return retVal;
	}
}