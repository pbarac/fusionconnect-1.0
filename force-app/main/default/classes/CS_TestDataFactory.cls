@IsTest
public with sharing class CS_TestDataFactory {


    public static cspmb__Price_Item__c createPriceItem(boolean dmlExecute ){
        cspmb__Price_Item__c pi = new cspmb__Price_Item__c();
        pi.Name = 'Test Product';
        pi.fc_IP_type__c = 'Static';
        pi.fc_tech_type__c = 'WiFi';
        pi.fc_on_network__c = 'N';
        pi.fc_vendor__c = 'Meraki';
        pi.fc_down_speed_str__c = '1.5 Gb';
        pi.cspmb__Product_Definition_Name__c = 'Access';
        pi.fc_prioritized_bandwidth__c = '100 Mbps';
        pi.fc_Advanced_Security__c = 'No';
        pi.fc_High_Availability__c = 'No';        
        pi.cspmb__Price_Item_Code__c = 'ABC-123 - 12MTH';
        pi.sku__c = 'ABC-123';
        pi.fc_sku__c = 'ABC-123';
        
        if(dmlExecute){
            insert pi;
        }
        return pi;        
    }

    public static cscrm__Address__c createAddress(boolean dmlExecute ){
        cscrm__Address__c addr = new cscrm__Address__c();
        addr.name = 'Test Address';
        addr.cscrm__City__c = 'City';
        addr.cscrm__State_Province__c = 'State';
        addr.cscrm__Country__c  = 'USA';
        addr.cscrm__Zip_Postal_Code__c = '99999';

        if(dmlExecute){
            insert addr;
        }        
        return addr;        
    }

    public static List<cscfga__Product_Basket__c> createBaskets(Integer numOfBaskets, String accountId, boolean dmlExecute ) {
        
        List<cscfga__Product_Basket__c> listOfBaskets = new List<cscfga__Product_Basket__c>();
        Opportunity opp = createOpportunity(accountId, true);
        if (numOfBaskets > 0) {
            for (Integer i = 0; i < numOfBaskets; i++) {
                cscfga__Product_Basket__c productBasket = new cscfga__Product_Basket__c();
                productBasket.Name = 'Test Basket' + i;
                productBasket.csordtelcoa__Account__c = accountId;
                productBasket.cscfga__Opportunity__c = opp.Id;
                listOfBaskets.add(productBasket);
            }
        }

        if(dmlExecute){
            insert listOfBaskets;
        }           

        return listOfBaskets;
    } 
    
    public static Account createAccount(boolean dmlExecute) {
        RecordTypeInfo accRecord = Schema.SObjectType.Account.RecordTypeInfosByName.get('Affiliate');
        Id accountRecord = accRecord.RecordTypeId;
        Account acc = new Account(RecordTypeId = accountRecord, Name = 'MegaPath Default');
        
        if(dmlExecute){
            insert acc;
        }   
        
        return acc;
     }
    
    public static Opportunity createOpportunity(String accountId, boolean dmlExecute) {
        csconta__Frame_Agreement__c fa = createFA(true);
        Opportunity opp = new Opportunity(Name = 'Test Opportunity',Selected_Pricebook__c='Test PriceBook', Selected_Frame_Agreement__c=fa.id,StageName='Prospecting', CloseDate=date.today());
        
        if(dmlExecute){
            insert opp;
        }   
        
        return opp;
     }

    public static cspmb__Pricing_Rule_Group__c createPRG(boolean dmlExecute) {
        cspmb__Pricing_Rule_Group__c prg = new cspmb__Pricing_Rule_Group__c(Name = 'Test PRG',cspmb__pricing_rule_group_code__c='Test PriceBook');
        
        if(dmlExecute){
            insert prg;
        }   
        
        return prg;
     }
    
     public static csconta__Frame_Agreement__c createFA(boolean dmlExecute) {
        cspmb__Pricing_Rule_Group__c prg = createPRG(true);
        csconta__Frame_Agreement__c fa = new csconta__Frame_Agreement__c(csconta__Agreement_Name__c = 'Test FA',PRGs__c=prg.id);
        
        if(dmlExecute){
            insert fa;
        }   
        
        return fa;
     }
}