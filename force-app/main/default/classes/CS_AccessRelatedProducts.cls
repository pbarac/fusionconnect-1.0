global class CS_AccessRelatedProducts implements cssmgnt.RemoteActionDataProvider {

    global static Map<String, Object> getData(Map<String, Object> inputMap) {

        List<cspmb__Price_Item__c> cpLst = new List<cspmb__Price_Item__c>();
        List<cspmb__Price_Item__c> pInstalLst = new List<cspmb__Price_Item__c>();
        List<cspmb__Price_Item__c> deviceLst = new List<cspmb__Price_Item__c>();                

        String contractTerm = (String) inputMap.get(CS_Constants.Related_Product_ContractTerm);
        String cpID = (String) inputMap.get(CS_Constants.Related_Product_cpID);
        // get the Default CPE value from the commercialProductID
        cspmb__Price_Item__c cpi = [Select Id, Name, fc_default_cpe__c from cspmb__Price_Item__c where Id =: cpID];
        
        cpLst = [Select Id, name, cspmb__Price_Item_Code__c, cspmb__recurring_charge__c, cspmb__one_off_charge__c, fc_cpe_type__c from cspmb__Price_Item__c where (Name =: CS_Constants.CP_Professional_Install and Contract_Term_Value__c =: contractTerm) OR (Name =: cpi.fc_default_cpe__c)];

        for(cspmb__Price_Item__c cp: cpLst){
            if(cp.Name == CS_Constants.CP_Professional_Install){
                pInstalLst.add(cp);    
            }else if(cp.Name ==  cpi.fc_default_cpe__c){
                deviceLst.add(cp);
            }
        }
        Map<String, Object> outputMap = new Map<String, Object>();
        outputMap.put('Professional Install', pInstalLst);
        outputMap.put('Device', deviceLst);
        return outputMap;
    }   

}