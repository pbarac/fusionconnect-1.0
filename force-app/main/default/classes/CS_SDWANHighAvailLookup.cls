/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANHighAvailLookup - Lookup for High Availability in solution management
*/
global class CS_SDWANHighAvailLookup extends cscfga.ALookupSearch {

    public static final String REQUIRED_ATTRIBUTES = '["Id", "Name", "SDWAN Type Value", "Prioritized bandwidth Value", "Advanced Security Value"]';

    // Attribute names used in the WHERE clause of SOQL query, JSON notation, e.g. '["Attribute1","Attribute2"]'
    public override String getRequiredAttributes() {
        return REQUIRED_ATTRIBUTES;
    }    
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){

        List<cspmb__Price_Item__c> commProdLst = new List<cspmb__Price_Item__c>();
        String techTypeValue = searchFields.get('SDWAN Type Value');        
        String pbValue = searchFields.get('Prioritized bandwidth Value');
        String advSecValue = searchFields.get('Advanced Security Value');
        
        List<AggregateResult> agr = [SELECT min(Id) cpid, fc_High_Availability__c FROM cspmb__Price_Item__c 
                                      WHERE cspmb__Product_Definition_Name__c = 'SD-WAN' and fc_tech_type__c =: techTypeValue and fc_prioritized_bandwidth__c =: pbValue and 
                                       fc_Advanced_Security__c =: advSecValue and fc_High_Availability__c != null 
                                        group by fc_High_Availability__c order by fc_High_Availability__c];
        
        for (AggregateResult ar : agr){
                cspmb__Price_Item__c cp = new cspmb__Price_Item__c(Id = string.Valueof(ar.get('cpid')), Name = string.Valueof(ar.get('fc_High_Availability__c')), fc_High_Availability__c = string.Valueof(ar.get('fc_High_Availability__c')));
                commProdLst.add(cp);
        }
        return commProdLst;
    }
}