public with sharing class CS_Manage_PRGs_Controller {

	public CS_Manage_PRGs_Controller(ApexPages.StandardController controller) {

	}

	@AuraEnabled(cacheable=true)
	public static Map<String, List<Object>> getUserSelectablePRGsAndFrameAgreements(String opportunityId) {
		// get available pricing rule groups from the local model
		List<cspmb__Pricing_Rule_Group__c> prgs = [
			SELECT name, cspmb__pricing_rule_group_code__c, Pricebook_Type__c
			FROM cspmb__Pricing_Rule_Group__c
			WHERE Pricebook_Type__c = 'Pricebook (catalogue)'
		];

		Map<String, List<Object>> prgsAndFrameAgreements = new Map<String, List<Object>>();
		prgsAndFrameAgreements.put('Pricebook (catalogue)', new List<cspmb__Pricing_Rule_Group__c>());
		prgsAndFrameAgreements.put('Promotion', new List<cspmb__Pricing_Rule_Group__c>());

		for (cspmb__Pricing_Rule_Group__c prg : prgs) {
			if (prg.Pricebook_Type__c == 'Pricebook (catalogue)') {
				prgsAndFrameAgreements.get(prg.Pricebook_Type__c).add(prg);
			}
		}

		List<String> selectedPrgs = getSelectedPRGs(opportunityId);
		if (selectedPrgs.size() > 0) {
			prgsAndFrameAgreements.put('selectedPricebook', selectedPrgs);
		}

		// frame agreements
		Opportunity opp = getOpportunity(opportunityId);
		List<csconta__Frame_Agreement__c> fas = [SELECT Id, csconta__Agreement_Name__c FROM csconta__Frame_Agreement__c WHERE csconta__Account__c = :opp.AccountId];
		prgsAndFrameAgreements.put('Frame agreement', fas);

		List<String> selectedFAs = getSelectedFAs(opportunityId);
		if (selectedFAs.size() > 0) {
			prgsAndFrameAgreements.put('selectedFa', selectedFAs);
		}

		return prgsAndFrameAgreements;
	}

	@AuraEnabled
	public static String recalculateBasketTotals(String basketId) {
		System.debug('Manage_PRGs_Controller.recalculateBasketTotals: ' + basketId);

		try {
			CS_PREConfigurationUpdater.recalculateBasketDiscounts(basketId);
		} catch (Exception e) {
			return e.getMessage();
		}

		return 'Done!';
	}

	@AuraEnabled
	public static void updateSelectedPRGsAndFAs(String opportunityId, String prg, String fa) {
		System.debug('Manage_PRGs_Controller.updateSelectedPRGs: ' + opportunityId + ', ' + prg + ', ' + fa);

		Opportunity opp = getOpportunity(opportunityId);

		opp.Selected_Pricebook__c = prg;
		opp.Selected_Frame_Agreement__c = fa;

		update opp;

		// get the selected prgs from the main configuration - attached on the "main" product configuration

		// List<cscfga__Attribute__c> catalogueCodeAttribute = [
		//     SELECT cscfga__Value__c from cscfga__Attribute__c
		//     WHERE cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basketId
		//     AND Name = 'Catalogue Code'
		//     LIMIT 1
		// ];

		// if (catalogueCodeAttribute.size() > 0) {
		//     catalogueCodeAttribute[0].cscfga__Value__c = String.join(prgs, ',');

		//     system.debug('Manage_PRGs_Controller.updateSelectedPRGs: Updating Main configuration attribute.');
		//     update catalogueCodeAttribute;
		// }
	}

	public static List<String> getSelectedPRGs(String opportunityId) {
		// get the selected prgs from the main configuration - attached on the "main" product configuration
		Opportunity opp = getOpportunity(opportunityId);

		return new List<String>{
			opp.Selected_Pricebook__c
		};
	}

	public static List<String> getSelectedFAs(String opportunityId) {
		// get the selected prgs from the main configuration - attached on the "main" product configuration
		Opportunity opp = getOpportunity(opportunityId);

		return new List<String>{
			opp.Selected_Frame_Agreement__c
		};
	}

	private static Opportunity getOpportunity(String opportunityId) {
		Opportunity opp = [
			SELECT Id, Selected_Pricebook__c, Selected_Frame_Agreement__c, AccountId
			FROM Opportunity
			WHERE Id = :opportunityId
			LIMIT 1
		][0];

		return opp;
	}
}