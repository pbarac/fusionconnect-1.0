/**
* @description CS_DeviceAddonsCPETypeLookup - Lookup for Device Addons Type in solution management
*/
global class CS_DeviceAddonsCPETypeLookup extends cscfga.ALookupSearch {

    public static final String REQUIRED_ATTRIBUTES = CS_Constants.REQUIRED_ATTRIBUTES_CS_DeviceAddonsCPETypeLookup;
    
    // Attribute names used in the WHERE clause of SOQL query, JSON notation, e.g. '["Attribute1","Attribute2"]'
    public override String getRequiredAttributes() {
        return REQUIRED_ATTRIBUTES;
    }    
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){

        List<cspmb__Price_Item__c> commProdLst = new List<cspmb__Price_Item__c>();
        
        List<AggregateResult> agr = [SELECT min(Id) cpid, fc_cpe_type__c FROM cspmb__Price_Item__c 
                                      WHERE cspmb__Product_Definition_Name__c =: CS_Constants.Product_Definition_Access and fc_cpe_type__c != null
                                      group by fc_cpe_type__c order by fc_cpe_type__c];
        system.debug('agr' +agr);
        for (AggregateResult ar : agr){
                cspmb__Price_Item__c cp = new cspmb__Price_Item__c(Id = string.Valueof(ar.get('cpid')), Name = string.Valueof(ar.get('fc_cpe_type__c')), fc_cpe_type__c = string.Valueof(ar.get('fc_cpe_type__c')));
                commProdLst.add(cp);
        }
        return commProdLst;
    }
}