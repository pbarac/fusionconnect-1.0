/**
* @description CS_AccessRelatedProducts_Test - Test coverage for class CS_AccessRelatedProducts
*/
@isTest 
public class CS_AccessRelatedProducts_Test{

    @isTest public static void getDataTest(){

        List<cspmb__Price_Item__c> cpiLst = new List<cspmb__Price_Item__c>();
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_cpe_type__c = 'Router';
        cpi.cspmb__Contract_Term__c = '12 Months';
        cpi.Name = 'Professional On-Site Install';
        cpiLst.add(cpi);

        cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_cpe_type__c = 'Router';
        cpi.cspmb__Contract_Term__c = '12 Months';
        cpi.Name = 'Access Product';
        cpi.fc_default_cpe__c = 'Device';        
        cpiLst.add(cpi);      

        cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_cpe_type__c = 'Router';
        cpi.cspmb__Contract_Term__c = '12 Months';
        cpi.Name = 'Device';
        cpiLst.add(cpi);   
        
        cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_cpe_type__c = 'Router';
        cpi.cspmb__Contract_Term__c = '12 Months';
        cpi.Name = 'Device';
        cpi.fc_default_cpe__c = 'Device';
        cpiLst.add(cpi);             
        insert cpiLst; 
        
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        inputMap.put('contractTerm', '12');
        inputMap.put('cpID', cpiLst[1].ID);        

        Test.startTest();
        outputMap = CS_AccessRelatedProducts.getData(inputMap);
        Test.stopTest();
        
        System.assertNotEquals(outputMap.get('Professional Install'), null, 'Professional Install map is not found');
        System.assertNotEquals(outputMap.get('Device'), null, 'Device map is not found');
    }
}