@isTest
private class CS_AccessNetworkTypeLookup_Test{
    
    @isTest static void testDoLookupSearch() {
        
        //create a commercial product
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_IP_type__c = 'Static';
        cpi.fc_tech_type__c = 'WiFi';
        cpi.fc_on_network__c = 'N';
        insert cpi;       

        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('IP Type Value', 'Static');
        searchFields.put('Tech Type Value', 'WiFi');

        Test.startTest();
        CS_AccessNetworkTypeLookup testObject = new CS_AccessNetworkTypeLookup();
        testObject.getRequiredAttributes();
        Object[] networkTypeCPLst = testObject.doLookupSearch(searchFields, null, null, null, null);
        Test.stopTest();

        System.assertNotEquals(networkTypeCPLst.size(), null, 'Network types are not retrieved');
        cpi = (cspmb__Price_Item__c) networkTypeCPLst[0];
        System.assertEquals(cpi.name, 'N', 'Network type is not correct');
    }
}