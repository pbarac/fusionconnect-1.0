@isTest
private class CS_AccessCarrierLookup_Test {
    
    @isTest static void testDoLookupSearch() {
        
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'Access';
        cpi.fc_IP_type__c = 'Static';
        cpi.fc_tech_type__c = 'WiFi';
        cpi.fc_on_network__c = 'N';
        cpi.fc_vendor__c = 'Meraki';
        insert cpi;       

        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('IP Type Value', 'Static');
        searchFields.put('Tech Type Value', 'WiFi');
        searchFields.put('Network Type Value', 'N');

        Test.startTest();
        CS_AccessCarrierLookup testObject = new CS_AccessCarrierLookup();
        testObject.getRequiredAttributes();
        Object[] carrierCPLst = testObject.doLookupSearch(searchFields, null, null, null, null);
        Test.stopTest();

        System.assertNotEquals(carrierCPLst.size(), null, 'Carrier Values are not retrieved');
        cpi = (cspmb__Price_Item__c) carrierCPLst[0];
        System.assertEquals(cpi.name, 'Meraki', 'Carrier is not correct');
    }
}