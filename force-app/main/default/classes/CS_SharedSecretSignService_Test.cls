@isTest public with sharing class CS_SharedSecretSignService_Test {
    @isTest public static void CS_SharedSecretSignService_Test() {
   
        Test.startTest();

        HttpRequest req = new HttpRequest();
   
        CS_SharedSecretSignService testObject = new CS_SharedSecretSignService();
        HttpRequest request =  testObject.getHeadersSharedSecret(req,'GET','https://example.com/example','{test}');
        Test.stopTest();

        System.assertNotEquals(request, null, 'Request not fetched');

    }
}