public with sharing class CS_AddressWrapper {

    public String addressLookup;
    public String name;
    public String street;
    public String streetName;
    public String city;
    public String state_province;
    public String zipCode;
    public String country;
    public Boolean validationFlag = false;
    public Boolean validationOverrideFlag = false;

    }


