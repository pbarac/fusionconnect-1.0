/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANPrioritizedBandwidthLookup_Test - Test coverage for class CS_SDWANPrioritizedBandwidthLookup
*/
@isTest 
public class CS_SDWANPrioritizedBandwidthLookup_Test{

    @isTest public static void doLookupSearchTest(){
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'SD-WAN';
        cpi.fc_hardware_model__c = 'Test Hardware model';
        insert cpi;
        
        Map<String,String> searchFields = new Map <String, String>();
        searchFields.put('SDWAN Type Value', 'WiFi');

        Test.startTest();
        CS_SDWANPrioritizedBandwidthLookup testObject = new CS_SDWANPrioritizedBandwidthLookup();
        testObject.getRequiredAttributes();        
        Object[] pbLst = testObject.doLookupSearch(searchFields,null,null,null,null);
        Test.stopTest();
        
        System.assertNotEquals(pbLst.size(), null, 'Prioritized Bandwidth Values are not fetched');
    }
}