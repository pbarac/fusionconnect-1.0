@isTest public with sharing class CS_CommercialProductPRELookup_Test {
    @isTest public static void CS_CommercialProductPRELookup_Test() {
        cspmb__Price_Item__c priceItem = CS_TestDataFactory.createPriceItem(true);
        priceItem.cspmb__Product_Definition_Name__c = 'SD-WAN';
        priceItem.fc_sku__c = 'ABC-123';

        update priceItem;
              
        Map<String, Object> inputMap = new Map<String, Object>{
            'sku' => priceItem.cspmb__Price_Item_Code__c
        };
        Test.startTest();
        CS_CommercialProductPRELookup testObj = new CS_CommercialProductPRELookup();
         Map<String, Object> data  = testObj.getData(inputMap);

        Test.stopTest();
       
        System.assert( data !=null);
    }
}