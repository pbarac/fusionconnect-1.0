public class CS_SharedSecretSignService {

	public HttpRequest getHeadersSharedSecret(HttpRequest request, String method, String endpoint, String payload) {
		Long csTime = System.currentTimeMillis();
        system.debug('organization=='+ CS_Constants.ORG_ID);
        system.debug('Authorization=='+ getAuthorizationHeader(method, endpoint, csTime));
        system.debug('CS-Signature=='+ getSignedPayload(payload));
        system.debug('CS-Time=='+ String.valueOf(csTime));
		request.setHeader('organization', CS_Constants.ORG_ID);
		request.setHeader('Authorization', getAuthorizationHeader(method, endpoint, csTime));
		request.setHeader('CS-Time', String.valueOf(csTime));
		request.setHeader('CS-Signature', getSignedPayload(payload));
		request.setHeader('Content-Type', 'application/json');

        //system.debug(request);
		return request;
	}

	private String getSignedPayload(String payload) {
		Blob digest = Crypto.generateDigest('sha1', Blob.valueOf(payload));
		String hexDigest = EncodingUtil.convertToHex(digest);
		Blob hash = Crypto.generateMac(
			'hmacSHA256',
			Blob.valueOf(hexDigest),
			Blob.valueOf(CS_Constants.CLIENT_SECRET)
		);

		return EncodingUtil.convertToHex(hash);
	}

	private String getAuthorizationHeader(String method, String endpoint, Long csTime) {
		String data = 'CS' + CS_Constants.CLIENT_ID + '\n' +
			csTime + '\n' +
			method.toUpperCase() + '\n' +
			endpoint + '\n';
		Blob hash = Crypto.generateMac(
			'hmacSHA256',
			Blob.valueOf(data),
			Blob.valueOf(CS_Constants.CLIENT_SECRET)
		);
		String hex = EncodingUtil.convertToHex(hash);
		String signature = EncodingUtil.base64Encode(Blob.valueOf(hex));

		return EncodingUtil.base64Encode(Blob.valueOf('CS' + CS_Constants.CLIENT_ID)) + ':' + signature;
	}
}