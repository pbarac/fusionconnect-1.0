global class CS_GetTechnical_Components implements cssmgnt.RemoteActionDataProvider {

    global static Map<String, Object> getData(Map<String, Object> inputMap) {
       
            system.debug('CommProductLookup input map:' + inputMap);    
            String cpId = (String) inputMap.get('CPId');

        	cspmb__Price_Item__c cpMaster = [select cspmb__Master_Price_item__c from cspmb__Price_Item__c where id = :cpId][0];

        	List<cspmb__Commercial_Product_Material_Group_Assoc__c> CPMGAList = [select id,name, cspmb__material_group__c, cspmb__commercial_product__c from cspmb__Commercial_Product_Material_Group_Assoc__c
            where cspmb__commercial_product__c = :cpMaster.cspmb__Master_Price_item__c];
            List<Id> MGIdList = new List<Id>();

            for(cspmb__Commercial_Product_Material_Group_Assoc__c CPMGA:CPMGAList) {
                MGIdList.add(CPMGA.cspmb__material_group__c);
            }
            List<cspmb__Material_Group_Material_Association__c> MGMAList = [
                select id,cspmb__material__r.name,cspmb__material__r.cspmb__code__c,cspmb__material__r.cspmb__type__c
                from cspmb__Material_Group_Material_Association__c
                where cspmb__material_group__c in :MGIdList];
            
            Map<String, Object> outputMap = new Map<String, Object>();
            outputMap.put('Technical Components',MGMAList);

            return outputMap;
                                                             
        }   

    }