@isTest
public with sharing class CS_AddressDuplicateCheck_Test {
    static testMethod void filterDuplicateAddressesTest() {

        cscrm__Address__c add = CS_TestDataFactory.createAddress(true);
        Account acc = CS_TestDataFactory.createAccount(true);

        CS_AddressWrapper addWrapper = new CS_AddressWrapper();
        addWrapper.street ='Test street';
        addWrapper.city = 'Test city';
        addWrapper.state_province = 'Test state_province';
        addWrapper.zipCode = 'Test zip code';
        addWrapper.country = 'Test country';


        List<CS_AddressWrapper> addWrapperList = new List<CS_AddressWrapper>();
        addWrapperList.add(addWrapper);

        Test.startTest();
        List<cscrm__Address__c> testResult = CS_AddressDuplicateCheck.filterDuplicateAddresses(addWrapperList,acc.id);
        System.assertNotEquals(null, testResult);
        Test.stopTest();

    }
}


