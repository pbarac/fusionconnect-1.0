/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANHardwareModelLookup_Test - Test coverage for class CS_SDWANHardwareModelLookup
*/
@isTest 
public class CS_SDWANHardwareModelLookup_Test{

    @isTest public static void doLookupSearchTest(){
        cspmb__Price_Item__c cpi = CS_TestDataFactory.createPriceItem(false);
        cpi.cspmb__Product_Definition_Name__c = 'SD-WAN';
        cpi.fc_default_cpe__c = 'ROUTER'; 
        insert cpi;
        
        Map<String,String> searchFields = new Map <String, String>();
        searchFields.put('Hardware Manufacturer Value', 'Meraki');         
        searchFields.put('High Availability Value', 'No');
        searchFields.put('SDWAN Type Value', 'WiFi');
        searchFields.put('Prioritized bandwidth Value', '100 Mbps');
        searchFields.put('Advanced Security Value', 'No');

        Test.startTest();
        CS_SDWANHardwareModelLookup testObject = new CS_SDWANHardwareModelLookup();
        testObject.getRequiredAttributes();        
        Object[] hmLst = testObject.doLookupSearch(searchFields,null,null,null,null);
        Test.stopTest();
        
        System.assertNotEquals(hmLst.size(), null, 'Hardware model Name values are not fetched');
    }
}