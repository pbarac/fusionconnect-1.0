import { LightningElement,api } from 'lwc';

import importAddressessTemplate from '@salesforce/resourceUrl/CS_ImportAddressesCSVTemplate';

export default class Cs_ImportAddressesCSV extends LightningElement {

    isExecuting = false;    
    @api async invoke() {
        if (this.isExecuting) {
            return;
        }  
        console.log('Execution Start');
        this.isExecuting = true;
        
        console.log('template static resource:');
        console.log(importAddressessTemplate);

        var downloadLink = document.createElement("a");
        downloadLink.href = window.location.origin + importAddressessTemplate;
        downloadLink.click();
        
       
        await this.sleep(2000);
        this.isExecuting = false;
        console.log('Execution Stop');
    }  sleep(ms) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
}