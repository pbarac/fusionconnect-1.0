import { LightningElement,track,api } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import importAddressesTemplate from '@salesforce/resourceUrl/CS_ImportAddressesCSVTemplate';
import getAddressRecords from "@salesforce/apex/CS_AddressValidationController.getAddressRecords";
import saveAddressRecords from "@salesforce/apex/CS_AddressValidationController.saveAddressRecords";
import getFieldsFromFieldSet from '@salesforce/apex/CS_AddressValidationController.getFieldsFromFieldSet';
import PARSER from '@salesforce/resourceUrl/PapaParse';
import NAME_FIELD from '@salesforce/schema/Account.Name';


const columns = [
    { label: 'Address Name', fieldName: 'name',type: 'text' },
    { label: 'Address1', fieldName: 'street',type: 'text'},
    { label: 'Address2', fieldName: 'streetName',type: 'text'},
    { label: 'Country', fieldName: 'country',type: 'text'},
    { label: 'State/Province', fieldName: 'state_province',type: 'text'},
    { label: 'City', fieldName: 'city',type: 'text'},
    { label: 'Zip Code', fieldName: 'zipCode',type: 'text'},
    { label: 'Validation Flag', fieldName: 'validationFlag', type: 'text'},
    {label: 'Validation Override Flag', fieldName: 'validationOverrideFlag', type: 'text'},
    {label: 'Selected Product', fieldName:'selectedProduct', type:'text'},
    {label:'NRR', fieldName:'nrr', type:'number'},
    {label:'MRR', fieldName:'mrr', type:'number'}
];

export default class Cs_AddressValidation extends LightningElement {
    parserInitialized = false;

    columns = columns;

    fields = [NAME_FIELD];
    @api recordId
    @api saveURL;

    @track testMessage;
    @track openaddAddressModal = false;
    @track openInfoModal = false;
    @track data = [];
    @track items = []; 
    @track addressRecords = [];
    
    @track page = 1; 
    @track startingRecord = 1;
    @track endingRecord = 0; 
    @track pageSize = 2; 
    @track totalRecountCount = 0;
    @track totalPage = 0;

    @track showSpinner = true;
    @track allValidAddressesFlag;
    @track invalidAddressesFlag
    @track duplicateAddressFlag;

    selectedAddressRows = [];
    fieldsApiFirstHalf = [];
    fieldsApiSecondHalf = [];
    accountId;
    invalidAddresses;
    

    connectedCallback(){
        console.log('save url :' + this.saveURL);
        getAddressRecords({
            recordId: this.recordId
            })
        .then(response =>{
            if(response){
                let objStr = JSON.parse(response);   
                this.addressRecords = JSON.parse(Object.values(objStr)[1]);
                this.accountId = Object.values(objStr)[0];
                console.log('account id ' + this.accountId);
                //this.addressRecords = JSON.parse(response);
                this.calculateTableRecordsPerPage();

            }else{
                console.log('Houston we have a problem')
            }
        })

        getFieldsFromFieldSet({ 
            objectApiName: 'cscrm__Address__c',
            fieldSetName: 'addAddressFS'
        })
        .then(data => {
            let items = []; 
           
            let objStr = JSON.parse(data);   
            let listOfFields = JSON.parse(Object.values(objStr)[1]);
            listOfFields.map(element=>items.push(element));      
          
            this.splitFieldsInHalf(items);                              
            this.error = undefined;   
        })
        .catch(error =>{
            this.error = error;
            console.log('error',error);                
        })
    }

    renderedCallback(){
        if(!this.parserInitialized){
            loadScript(this, PARSER)
                .then(() => {
                    console.log('Parser initialized');
                    this.parserInitialized = true;
                })
                .catch(error => console.error(error));
        }
    }

    splitFieldsInHalf(inputFiledsApi){
        const half = Math. ceil(inputFiledsApi. length / 2);
        this.fieldsApiFirstHalf = inputFiledsApi. splice(0, half)
        this.fieldsApiSecondHalf = inputFiledsApi. splice(-half)
        for(let v in this.fieldsApiFirstHalf){
            if(this.fieldsApiFirstHalf[v].fieldPath =="cscrm__Account__c")
            {
                this.fieldsApiFirstHalf[v].value = this.accountId;
            }else{
                this.fieldsApiFirstHalf[v].value='';
            }
        }
    }

    calculateTableRecordsPerPage(){							 
        this.data = this.addressRecords.slice();
    }

    showAddAddressModal() {
        this.openaddAddressModal = true;
    }
    closeAddAddressModal() {
        this.openaddAddressModal = false;
    }

    showImportCsvModal(){
        this.openInfoModal = true;
    }
    closeImportCsvModal(){
        this.openInfoModal = false;
    }

    handleSubmit(event){
        event.preventDefault();       // stop the form from submitting
        const fields = event.detail.fields;
													 
        let temp = [];
        let newAddress = {
            name:fields.cscrm__State_Province__c  + '; ' + fields.cscrm__Country__c +'; ' + fields.cscrm__City__c + '; ' + fields.cscrm__Street__c + '; ' + fields.cscrm__Zip_Postal_Code__c,
            street:fields.cscrm__Street__c,
            streetName:fields.cscrm__Street_Name__c,
            city:fields.cscrm__City__c,
            state_province:fields.cscrm__State_Province__c,
            zipCode:fields.cscrm__Zip_Postal_Code__c,
            country:fields.cscrm__Country__c
        };

        temp.push(newAddress);

        saveAddressRecords({ base64Data: JSON.stringify(temp), accountId: fields.cscrm__Account__c})
        .then(data => {
            this.showAddressRecordsOnUI(data,fields.cscrm__Account__c, 'SINGLE');
          })
    }  

    getSelectedRows(event){
        let selectedArray = [];
        this.selectedAddressRows=[];
        console.log('selecting rows...');
        const selectedRows = event.detail.selectedRows;
        // Display that fieldName of the selected  rows
        for (let i = 0; i < selectedRows.length; i++) {
            console.log('you selected: ' + selectedRows[i].street);
            //if(selectedRows[i].validationFlag || selectedRows[i].validationOverrideFlag){
                selectedArray.push(selectedRows[i]);
            //}
        }
        this.selectedAddressRows.push(...selectedArray);
        console.log('selected add row');
        console.log(this.selectedAddressRows);
    }

    validateAddresses(){
        console.log('addresses for validation: ');
        console.log(this.selectedAddressRows);
    }

    previousHandler() {
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }

    nextHandler() {
        if((this.page<this.totalPage) && this.page !== this.totalPage){
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);            
        }             
    }

    displayRecordPerPage(page){
        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);

        this.endingRecord = (this.endingRecord > this.totalRecountCount) 
                            ? this.totalRecountCount : this.endingRecord; 

        this.data = this.items.slice(this.startingRecord, this.endingRecord);

        this.startingRecord = this.startingRecord + 1;
    }  

    handleImportCSVAsync(file){
        this.allValidAddressesFlag = false;
        this.invalidAddressesFlag = true;
        
        console.log('file is uploaded');
        //const file = e.target.files[0];
        return new Promise ((resolve,reject) => {
            Papa.parse(file, {
                quoteChar: '"',
                header: 'true',
                complete: (results) => {
                    this.showSpinner = false;
                    resolve(results.data);
                },
                error: (error) => {
                    reject(error);
                    console.error('error ' + error);
                }
            })

        });          
    }

    async handleImportCSV(event){
        const file = event.target.files[0];
        this.openInfoModal = true;
        this.showSpinner = true;
        let importAddressArray = await this.handleImportCSVAsync(file).catch((error) => {
            console.log(error);
        });


        let validAddresses = await this.filterValidAddresses(importAddressArray); 
        this.invalidAddresses =  importAddressArray.filter(item => {
            if(!validAddresses.includes(item) && (!(item.street && item.city && item.state_province && item.zipCode && item.country) && (item.street || item.city || item.state_province || item.zipCode || item.country))){
                return item;
            }
            
        });

        console.log('invalid addre');
        console.log(this.invalidAddresses)
        if(this.invalidAddresses.length === 0){
            this.allValidAddressesFlag = true;
            this.invalidAddressesFlag = false;  
        }

    
        saveAddressRecords({ base64Data: JSON.stringify(validAddresses), accountId: this.accountId})
         .then(data => {

            this.showAddressRecordsOnUI(data,this.accountId, 'BULK');

          })
        }

    showAddressRecordsOnUI(data, accId, typeOfInsertion){
        let objStr = JSON.parse(data); 

        if(Object.values(objStr)[1] === '200') {
            console.log('response = 200');
            if(this.accountId === accId){
                let newAddressArray = [];
                
                newAddressArray.push(...JSON.parse(Object.values(objStr)[0]));
                newAddressArray.push(...this.addressRecords);
                this.addressRecords = newAddressArray.slice();
                this.calculateTableRecordsPerPage();
            }
        }else{
            if(typeOfInsertion === 'SINGLE'){
                this.showSpinner = false;
                this.openInfoModal = true;
                this.duplicateAddressFlag = true;
                this.allValidAddressesFlag = false;

            }

            if(typeOfInsertion ==='BULK'){
                this.showSpinner = false;
                this.openInfoModal = true;
                this.invalidAddressesFlag= true;
                this.duplicateAddressFlag = false;
                this.allValidAddressesFlag = false;

            }
        }
        this.openaddAddressModal = false;
        
    }

    filterValidAddresses(addressList){
        return new Promise ((resolve,reject) => {
            const validAddresses = addressList.filter( add => {
                console.log('add ' + add);
                if(add.street  && add.city && add.state_province && add.zipCode && add.country){
                    add.name = add.state_province + '; ' + add.country + '; ' + add.city + '; ' + add.street + '; ' + add.zipCode;
                    return add;
                }            
            });
            if(validAddresses){
                resolve(validAddresses);
            }else{
                reject;
            }
           
        });
    }

    downloadCSVTemplate(){
        var downloadLink = document.createElement("a");
        downloadLink.href = window.location.origin + importAddressesTemplate;
        downloadLink.click();

    }

    createConfigs(event){
        let parentWin = window.parent;
        let objToPass = {'selectedAddresses': JSON.stringify(this.selectedAddressRows)};
        parentWin.postMessage({
            command: 'CREATE CONFIGURATIONS',
            data: objToPass
        }, this.saveURL)
    }

}