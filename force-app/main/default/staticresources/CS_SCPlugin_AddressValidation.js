console.log('Loaded SC Anndress Validation plugin');

if(!CS || !CS.SM){
    throw Error('Solution Console Api not loaded?');
}

window.document.addEventListener('SolutionConsoleReady', async function () {
    
    if (CS.SM.registerUIPlugin) {
        await CS.SM.registerUIPlugin();
        console.log('Petra:UI PlugIn Registered');

        const location = window.location;
        const hostname = location.hostname;
        //const pathname = location.pathname;
        const queryString = location.search;
        const urlParams = new URLSearchParams(queryString);
        let basketId = urlParams.get('basketId');
        let originWindow = window.origin;
        console.log( 'basketId FIX: ' + basketId);


        const customComponentURL =`/apex/c__CS_AddressValidation?id=${basketId}&saveURL=${originWindow}`;
        console.log('this is cmp url ' + customComponentURL);

        
        CS.SM.UIPlugin.afterNavigate = async (currentComponent, previousComponent) => {
            if(currentComponent && currentComponent.name){
                if(currentComponent.name == 'Address Check'){
                    setTimeout(() => {
                        let componentDiv = document.getElementById('custom-component-div');
                        componentDiv.innerHTML= `<iframe width="100%" height="515"src="${customComponentURL}"></iframe>`
                    },150)
                }
            }
        }


        window.addEventListener('message', handleIframeMessage);

        async function handleIframeMessage(e){
            const solution = await CS.SM.getActiveSolution();
            console.log('active solution: '+ solution);
			const component = solution.getComponentByName('Connectivity Solution');
            console.log('comp: '+ component);
            console.log('handle Iframe message: '+ e.data['data'].selectedAddresses);
        }

    }
});