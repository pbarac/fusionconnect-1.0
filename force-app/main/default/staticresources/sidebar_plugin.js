var tracker = 0;

console.log('Loaded SC sidebar plugin Prod');
if (!CS || !CS.SM) {
	throw Error('Solution Console Api not loaded?');
}

if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', async function (event) {
		console.log('Registering SC sidebar plugin');

		let solutionPlugin = await CS.SM.registerPlugin('Sidebar plugin');
		SidebarPlugin.updatePlugin(solutionPlugin);
	});
}

SidebarPlugin = {
	updatePlugin: function(solutionPlugin) {
		updatePlugin();
	}
};

(function () {
	const link = document.createElement('link');
	link.rel = 'stylesheet';
	link.type = 'text/css';
	link.href = 'https://fusionconnect--cpqdev.lightning.force.com/resource/' + (new Date()).getTime() + '/sidebar_css/css/main.css';

	// Get HTML head element to append
	// link element to it
	document.getElementsByTagName('HEAD')[0].appendChild(link);
})();

function updatePlugin() {
	function financial(x) {
		if (typeof x === 'undefined') {
			return '-';
		}
		return Number.parseFloat(x).toFixed(2);
	}

	function percentage(sales, list) {
		//const decimalOff = Math.floor(((list - sales) * 100) / list);
		const decimalOff = Math.round(100 - ((100 * sales) / list));

		if (decimalOff === 0 || decimalOff === Infinity || decimalOff === -Infinity || decimalOff < 0) {
			return 0;
		}

		return decimalOff;
	}

	function paginateArray(array, page_number, page_size) {
		if (isNaN(page_number))
			page_number = 1
		--page_number; // because pages logically start with 1, but technically with 0
		return array.slice(page_number * page_size, (page_number + 1) * page_size);
	  }

	async function createSidebarHtml(cart, page, pageSize) {
		function getSelectedPage() {
			try {
				const page = document.querySelectorAll('#pricing-summary-pagination .pagination-buttons-wrapper > button.pager-selected')[0].innerText;
				return parseInt(page);
			} catch (e) {
				return 1;
			}
		}

		function getSelectedPageSize() {
			try {
				const pageSize = document.querySelectorAll('#pricing-summary-pagination .slds-select')[0].selectedOptions[0].value;
				return parseInt(pageSize);
			} catch (e) {
				return 10;
			}
		}

		function getSolutionConfigurations(solution) {

			const allConfigs = solution.schema.configurations && Object.values(solution.schema.configurations) || [];
			return solution.components && Object.values(solution.components).reduce((acc, curr) => {
				const relatedProducts = curr.schema.configurations && Object.values(curr.schema.configurations)
					.map(config => config.relatedProductList)
					.reduce((acc, curr) => {
						return acc.concat(curr.map(relatedProduct => {
							return {
								...relatedProduct.configuration,
								isRelatedProduct: true
							}
						}));
					}, []);
	
				return Object.values(acc).concat(Object.values(curr.schema.configurations)).concat(relatedProducts);
			}, allConfigs);
		}

		let solution;
		try {
			console.log(new Date().toISOString() + ' getting solution');
			solution = await CS.SM.getActiveSolution();
			console.log(new Date().toISOString() + ' got solution');
		} catch (e) {
			throw 'No solution found!';
		}

		page = page || getSelectedPage();
		pageSize = pageSize || getSelectedPageSize();
		const allSolutionConfigurations = getSolutionConfigurations(solution);
		const solutionConfigurations = paginateArray(allSolutionConfigurations, page, pageSize);

		console.log('numOfConfigs ' + solutionConfigurations.length);

		const numOfPages = Math.ceil(allSolutionConfigurations.length / 10);

		const tableRows = (cartItems) => {
			return solutionConfigurations.map(config => {
				if (config.isRelatedProduct) {
					return null;
				}
				const configName = config.configurationName;
				const cartItem = cartItems.find(it => it.id === config.guid) || { pricing: {} };

				const totalListOneOffPrice = financial(cartItem.pricing.totalListOneOffPrice);
				const totalListRecurringPrice = financial(cartItem.pricing.totalListRecurringPrice);

				const oneOffPercentage = percentage(cartItem.pricing.totalSalesOneOffPrice, cartItem.pricing.totalListOneOffPrice);
				const recurringPercentage = percentage(cartItem.pricing.totalSalesRecurringPrice, cartItem.pricing.totalListRecurringPrice);

				const totalSalesOneOffPrice = financial(cartItem.pricing.totalSalesOneOffPrice);
				const totalSalesRecurringPrice = financial(cartItem.pricing.totalSalesRecurringPrice);

				const item = {
					configName: configName,
					totalListOneOffPrice: totalListOneOffPrice,
					totalListRecurringPrice: totalListRecurringPrice,
					totalSalesOneOffPrice: totalSalesOneOffPrice,
					totalSalesRecurringPrice: totalSalesRecurringPrice,
					pricing: cartItem.pricing
				};

				console.log({item})
				return `
				<div class="pricing-sidebar-table-row-wrapper" data-row-item='${JSON.stringify(item)}' onmouseover="CS.drawOnHoverPopup(this)">
					<div class="pricing-sidebar-table-row">
						<div>${configName}</div>
						<div></div>
						<div>
							<span>List Pr.</span>
							<span>${totalListRecurringPrice}</span> <!-- recurring list price -->
						</div>
						<div>${totalListOneOffPrice}</div> <!-- oneOff list price -->
					</div>
					<div class="pricing-sidebar-table-row active">
						<div>x1</div>
						<div>
							${(
								oneOffPercentage ? 
								'<button class="button-primary"><b>' + oneOffPercentage + '%</b> off</button>' 
								: (
									recurringPercentage ? '<button class="button-primary"><b>' + recurringPercentage + '%</b> off</button>' : 
									'<button style="visibility: hidden;" class="button-primary"><b>0%</b> off</button>'
								  )
								)}
						</div>
						<div>
							<span>Sales Pr.</span>
							<span>${totalSalesRecurringPrice}</span> <!-- recurring sales price -->
						</div>
						<div>${totalSalesOneOffPrice}</div> <!-- oneOff sales price -->
					</div>
				</div>`;
			}).filter(Boolean).join(' ');
		};

		const sidebarTable =
			`<div class="pricing-sidebar-table">
				<div class="pricing-sidebar-table-head">
					<h5>Product</h5>
					<h5>Discount</h5>
					<h5>Recurring</h5>
					<h5>One Off</h5>
				</div>
				<div class="pricing-sidebar-table-rows">
					${tableRows(cart.items)}
				</div>
			</div>`;

	
		const pricingDetailsButton = 
		
		`<div class="section">
				<!-- <button class="button button-lg" id="pricing-details-button">Pricing Details Overview</button> -->
			</div>`;

		const pagination = 

		`
			<nav id="pricing-summary-pagination" aria-label="pagination" class="pagination-wrapper ng-star-inserted">
				<div class="pagination-buttons-wrapper">
					${
						(page - 1 === 0) ?
							`<button aria-label="previous page" class="cs-btn navigation-button icon-down" disabled ></button>` :
							`<button aria-label="previous page" class="cs-btn navigation-button icon-down" onclick="CS.onClickPaginate(${page-1})"></button>`
					}
					${
						[...Array(numOfPages + 1).keys()].map((num) => {
							if (num === 0) {
								return null;
							}

							if ((num >= (page - 2)) && (num <= (page + 2))) {
								if (num === page) {
									return `<button class="cs-btn pager-button ng-star-inserted pager-selected" onclick="CS.onClickPaginate(${num})">${num}</button>`
								} else {
									return `<button class="cs-btn pager-button ng-star-inserted" onclick="CS.onClickPaginate(${num})">${num}</button>`
								}
							}
						}).filter(Boolean).join(' ')
					}
					${
						(page + 1 > numOfPages) ?
							`<button aria-label="next page" class="cs-btn navigation-button icon-down" disabled ></button>` :
							`<button aria-label="next page" class="cs-btn navigation-button icon-down" onclick="CS.onClickPaginate(${page+1})"></button>`
					}
				</div>
				<!--
				<div class="slds-form-element__control page-size-container ng-star-inserted">
					<div aria-label="items per page" class="slds-select_container">
						<select class="slds-select ng-untouched ng-pristine ng-valid">
							<option value="10">10</option>
							<option value="25">25</option>
							<option value="50">50</option>
							<option value="100">100</option>
						</select>
					</div>
				</div>
				-->
			</nav>
			`;


		return `${sidebarTable}
			${pagination}`;
	}

	function onClickPaginate(num) {
		drawSidebar(CS.currentPRECart, num);
	}

	function removePrevElms(){

		var elms = document.querySelectorAll('.pricing-sidebar-table');
		elms.forEach(elm => elm.remove());

		elms = document.querySelectorAll('.pagination-buttons-wrapper');
		elms.forEach(elm => elm.remove());
			
	}

	async function drawSidebar(cart, page, pageSize) {
		tracker++;
		try {
			// remove warning message
			document.querySelectorAll('.pricing-summary-wrapper .warning-message')[0].outerHTML = '';
		} catch (e) {  } 

		try {
			const sidebarHtml = await createSidebarHtml(cart || {}, page, pageSize);

			console.log('start setting inner html: ' + new Date().toISOString());

			//document.querySelectorAll('.pricing-summary-table .item-list-wrapper')[0].innerHTML = '';
			//document.querySelectorAll('.pricing-summary-table .item-list-wrapper')[0].innerHTML = sidebarHtml;

			//document.querySelectorAll('.pricing-summary-table .component-list')[0].innerHTML = '';
			//document.querySelectorAll('.pricing-summary-table .component-list')[0].innerHTML = sidebarHtml;

			//^^ At this point, these are depreciated but I am going to leve them here just in case

			removePrevElms();

			document.querySelectorAll('.pricing-summary-table .component-list')[0].style.display = 'none'; //this will remove the top element.
			document.querySelectorAll('.pricing-summary-table .component-list')[0].parentElement.insertAdjacentHTML("beforeend", sidebarHtml);
			



			console.log('done  setting inner html: ' + new Date().toISOString());
		} catch (e) {
			console.warn(e);
		}


		// a container for the modal to be displayed in, should be hidden
		try {
			const modalContainerWrapper = document.createElement('div');
			modalContainerWrapper.style.position = 'relative';
			modalContainerWrapper.className = 'modal-container-wrapper';

			const modalContainer = document.createElement('div');
			modalContainer.style.width = '400px';
			modalContainer.style.height = '100%';
			modalContainer.style.position = 'absolute';
			modalContainer.style.top = '0';
			modalContainer.style.right = '0';
			modalContainer.style.display = 'none';
			// modalContainer.style.pointerEvents = 'none';
			modalContainer.className = 'modal-container';

			modalContainerWrapper.appendChild(modalContainer);


			// add the modal-container-wrapper only if it does not exist
			if (!document.querySelectorAll('.modal-container-wrapper').length) {
				//const sibling = document.querySelectorAll('.pricing-summary-wrapper')[0];
				const sibling = document.querySelectorAll('.solution-wrapper')[0];
				const parent = sibling.parentElement;
				parent.insertBefore(modalContainerWrapper, sibling.nextSibling);
				//parent.insertBefore(modalContainerWrapper, sibling);
				
			}
		} catch (e) {
			console.error(e);
		}
	}

	CS.drawSidebar = drawSidebar;
	CS.drawOnHoverPopup = drawOnHoverPopup;
	CS.hideOnHoverPopup = hideOnHoverPopup;
	CS.onClickPaginate = onClickPaginate;

	/*********
	 * POPUP *
	 *********/
	function hideOnHoverPopup() {
		document.querySelectorAll('.modal-container')[0].style.display = 'none';
		document.querySelectorAll('.modal-container')[0].innerHTML = '';
	}

	function drawOnHoverPopup(element) {
		const item = JSON.parse(element.attributes["data-row-item"].value);
		const discounts = item && item.pricing && item.pricing.discounts || [];

		function generateDiscountHtml(discount) {

			const prg = JSON.parse(RetrievePRGData.prToPrgMap[discount.source]);
			return `
				<div class="pricing-sidebar-table-row active">
					<div>${prg.name || ''}</div>
					<div></div>
					<div>
						${discount.type === 'percentage' ?
							'<button class="button-primary"><b>' + discount.amount + '%</b> off</button>' :
							'<button style="visibility: hidden;" class="button-primary"></button>'
						}
					</div>
					${discount.chargeType === 'recurring' ?
						(discount.type === 'percentage' ? '<div>-</div><div>-</div>' : '<div>' + financial(discount.amount) +'</div><div>-</div>' ):
						'<div>-</div><div>' + financial(discount.amount) +'</div>'
					}

				</div>
			`;
		}

// Commenting as generateDiscountHtml is causing a repetitive error
		/*
		const discountsHtml = discounts.reduce((curr, d) => {
			if (d.memberDiscounts && d.memberDiscounts.length > 0) {
				d.memberDiscounts.forEach(md => curr.push(generateDiscountHtml(md)));
			} else {
				curr.push(generateDiscountHtml(d));
			}
			return curr;
		}, []).join('');
		*/
		const discountsHtml = ""

		const modal = `
		<!-- Pricing Modal -->
		<div class="pricing-modal" onmouseleave="CS.hideOnHoverPopup()">
			<div class="pricing-modal-head">
				<h3>Pricing Details Drilldown</h3>
			</div>
			<div class="pricing-modal-table">
				<div class="pricing-sidebar-table">
					<div class="pricing-sidebar-table-head">
						<h5>Product</h5>
						<h5>Quantity</h5>
						<h5>Discount</h5>
						<h5>Recurring</h5>
						<h5>One Off</h5>
					</div>
					<div class="pricing-sidebar-table-rows">
						<div class="pricing-sidebar-table-row">
							<div>${item.configName}</div>
							<div>x1</div>
							<div></div>
							<div>${item.totalListRecurringPrice}</div> <!-- recurring list price -->
							<div>${item.totalListOneOffPrice}</div> <!-- one off list price -->
						</div>
						<div class="pricing-sidebar-table-row">
							<div class="pricing-sidebar-table-total">Charges subtotal</div>
							<div></div>
							<div></div>
							<div class="pricing-sidebar-table-total">${item.totalListRecurringPrice}</div>
							<div class="pricing-sidebar-table-total">${item.totalListOneOffPrice}</div>
						</div>
						<!-- DISCOUNTS -->
						${discountsHtml || null}
						<div class="pricing-sidebar-table-row">
							<div class="pricing-sidebar-table-total">Totals</div>
							<div></div>
							<div></div>
							<div class="pricing-sidebar-table-total">${item.totalSalesRecurringPrice}</div> <!-- recurring sales price -->
							<div class="pricing-sidebar-table-total">${item.totalSalesOneOffPrice}</div> <!-- one off sales price -->
						</div>
					</div>
				</div>
			</div>
			<div class="pricing-modal-footer">
				<button class="button" id="pricing-modal-close" onclick="CS.hideOnHoverPopup()">Close</button>
			</div>
		</div>`;

		// display after creating html
		document.querySelectorAll('.modal-container')[0].style.display = 'block';
		document.querySelectorAll('.modal-container')[0].innerHTML = modal;
	}
}
