console.log('Loaded SC Connectivity Solution plugin');
if (!CS || !CS.SM) {
	throw Error('Solution Console Api not loaded?');
}

const ConnectivitySolutionConstants = {
	mainSolutionName: 'Connectivity Solution',
	accessComponentName: 'Access',
	SDWANComponentName: 'SD-WAN',
	catalogueCodeAttributeName: 'CatalogueCode',
	promoCodeAttributeName: 'PromoCode',
	preDiscountsAttributeName: 'PRE Discounts',
	recurringAttributeName: 'MRC',
	oneOffAttributeName: 'NRC',
	childRecurringAttributeName: 'MRR',
	childOneOffAttributeName: 'NRR',
	quantity: 'Quantity'
};

let RetrievePRGData = {
	prToPrgMap: undefined
};

if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', async function (event) {
		console.log('Registering SC Connectivity Solution plugin');

		let solutionPlugin = await CS.SM.registerPlugin(ConnectivitySolutionConstants.mainSolutionName);
		ConnectivitySolutionPlugin.updatePlugin(solutionPlugin);
	});

	window.document.addEventListener('SolutionSetActive', async function (event) {
		console.log('SolutionSetActive', event);

		const solutionName = event.detail.solution.name;
		if (solutionName === ConnectivitySolutionConstants.mainSolutionName) {
			ConnectivitySolutionPlugin.setBasketId();

			await ConnectivitySolutionPlugin.getPrToPrgMap();
			await ConnectivitySolutionPlugin.afterSolutionLoaded();
			await ConnectivitySolutionPlugin.displayCurrency();
		}
	});
}

(function() {
	var link = document.createElement('link');
	link.rel = 'stylesheet';
	link.type = 'text/css';
	link.href = 'https://cs-pe.lightning.force.com/resource/' + (new Date()).getTime() + '/sol_con_custom_css';

	// Get HTML head element to append
	// link element to it
	document.getElementsByTagName('HEAD')[0].appendChild(link);
})();

ConnectivitySolutionPlugin = {
	solution: undefined,
	basketId: undefined,
	// setting the basketId used within the plugin. CS.SM.getActiveBasketId() doesn't seem to be working.
	setBasketId: function () {
		const basketsMap = CS.SM.solutionManager && CS.SM.solutionManager.baskets;
		this.basketId = basketsMap && Object.keys(basketsMap) && Object.keys(basketsMap)[0];
	},
	// getting the default user currency and setting it for the total solution value in the sidebar
	displayCurrency: async function () {
		const res = await ConnectivitySolutionPlugin.performRemoteAction(
			'CS_UserSettingProvider', {}
		);

		const currencySymbol = res && res.currencySymbol;
		if (!currencySymbol) {
			return;
		}

		const currencyElems = document.querySelectorAll('.pricing-table-header .price .total-currency');
		const currencyElem = currencyElems && currencyElems[0];

		currencyElem && (currencyElem.innerText = currencySymbol);
	},
	// propagates Contract Term from Main Solution component to all other components (if attribute exits)
	propagateContractTermToComponents: async function () {
		console.log('propagateContractTermToComponents');
		let solution = ConnectivitySolutionPlugin.solution;

		if (!solution) {
			try {
				ConnectivitySolutionPlugin.solution = await CS.SM.getActiveSolution();
				solution = ConnectivitySolutionPlugin.solution;
			} catch(e) {
				console.error('Failed loading active solution!', e);
			}
		}

		//Get solutions's Contract Term value
		const newValue = ConnectivitySolutionPlugin.getSmAttributeValue(
			solution, ConnectivitySolutionConstants.mainSolutionName, 'Contract Term'
		);

		if (newValue) {
			await ConnectivitySolutionPlugin.setSmAttributeValues(solution, 'Contract Term', newValue);
		}
	},
	updateConfigurationAttributesForComponent: async function (compName, updateMap, skipHooks = false) {

		const component = await ConnectivitySolutionPlugin.solution.getComponentByName(compName);

		return Promise.all(
			Object.keys(updateMap).map(
				key => component.updateConfigurationAttribute(key, updateMap[key], skipHooks)
			)
		);
	},
	performRemoteAction: async function(lookupClassName, inputMap) {
		const basket = await CS.SM.getActiveBasket();
		return basket.performRemoteAction(lookupClassName, inputMap);
	},
	getPrToPrgMap: async function getPrToPrgMap() {
		if (!RetrievePRGData.prToPrgMap) {
			RetrievePRGData.prToPrgMap = await ConnectivitySolutionPlugin.performRemoteAction(
			'CS_PRG_Data_Retriever', {}
			);
		}
	},
	ClassWatcher: class ClassWatcher {
		constructor(targetNode, classToWatch, classAddedCallback, classRemovedCallback) {
			this.targetNode = targetNode;
			this.classToWatch = classToWatch;
			this.classAddedCallback = classAddedCallback;
			this.classRemovedCallback = classRemovedCallback;
			this.observer = null;
			this.lastClassState = targetNode.classList.contains(this.classToWatch);

			this.init();
		}

		init() {
			this.observer = new MutationObserver(this.mutationCallback);
			this.observe();
		}

		observe() {
			this.observer.observe(this.targetNode, { attributes: true });
		}

		disconnect() {
			this.observer.disconnect();
		}

		mutationCallback = mutationsList => {
			for (let mutation of mutationsList) {
				if (mutation.type === 'attributes' && mutation.attributeName === 'class') {
					let currentClassState = mutation.target.classList.contains(this.classToWatch);
					if (this.lastClassState !== currentClassState) {
						this.lastClassState = currentClassState;
						if (currentClassState) {
							this.classAddedCallback();
						} else {
							this.classRemovedCallback();
						}
					}
				}
			}
		}
	},
	getSolutionConfigurations: function getSolutionConfigurations(solution) {

		const allConfigs = solution.schema.configurations && Object.values(solution.schema.configurations) || [];

		return solution.components && Object.values(solution.components).reduce((acc, curr) => {
			const relatedProducts = curr.schema.configurations && Object.values(curr.schema.configurations)
				.map(config => config.relatedProductList)
				.reduce((acc, curr) => {
					return acc.concat(curr.map(relatedProduct => {
						return {
							...relatedProduct.configuration,
							isRelatedProduct: true
						}
					}));
				}, []);

			return Object.values(acc).concat(Object.values(curr.schema.configurations)).concat(relatedProducts);
		}, allConfigs);
	},
	MutationObserver: undefined,
	afterSolutionLoaded: async function afterSolutionLoaded(solution) {
		console.log('Solution Loaded!');

		ConnectivitySolutionPlugin.solution = solution;
		if (!ConnectivitySolutionPlugin.solution) {
			try {
				ConnectivitySolutionPlugin.solution = await CS.SM.getActiveSolution();
			} catch(e) {
				console.error('Failed loading active solution!', e);
			}
		}

		// hide the deal management button if a frame agreement is selected
		const attrValue = ConnectivitySolutionPlugin.getSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'Frame Agreement');
		if (attrValue) {
			//document.querySelectorAll('#redirectToDealManagement').forEach(elem => { elem.style.display = 'none' })
		}

		if (ConnectivitySolutionPlugin.MutationObserver) {
			console.log('Removing mutation observer!');
			ConnectivitySolutionPlugin.MutationObserver.disconnect();
			ConnectivitySolutionPlugin.MutationObserver = undefined;
		}

		if (!ConnectivitySolutionPlugin.MutationObserver) {
			console.log('Adding mutation observer!');

			const target = document.getElementsByClassName('pricing-summary-wrapper')[0];
			ConnectivitySolutionPlugin.MutationObserver = target && new ConnectivitySolutionPlugin.ClassWatcher(
				target,
				'collapsed',  // class to watch out for
				() => { /* when it is collapsed do nothing */ },
				() => {
					ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE();
				}
			) || undefined;
		}

		// load the account Id and put it in a attribute to be used
		// in the frame agreement lookup
		await ConnectivitySolutionPlugin.getDefaultPRGAndFa();
		await ConnectivitySolutionPlugin.wait();
		await ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE();
	},
	getSmAttributeValue: function getSmAttributeValue(solution, configName, attributeName) {
		const solutionConfigurations = ConnectivitySolutionPlugin.getSolutionConfigurations(solution);

		const config = solutionConfigurations.find(c => {
			return c.configurationName.trim() === configName;
		});
		if (!config) {
			return '';
		}

		const attribute = Object.values(config.attributes).find((at) => {
			return at.name === attributeName;
		});

		if (!attribute) {
			return '';
		}

		return attribute.value;
	},
	// Returns a function, that, as long as it continues to be invoked, will not
	// be triggered. The function will be called after it stops being called for
	// N milliseconds. If `immediate` is passed, trigger the function on the
	// leading edge, instead of the trailing.
	debounceF: function debounceF(func, wait, immediate) {
		var timeout;
		return function () {
			var context = this, args = arguments;
			var later = function () {
				timeout = null;
				if (!immediate) {
					func.apply(context, args);
				}
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) {
				func.apply(context, args);
			}
		};
	},
	wait: function wait() {
		return new Promise(resolve => {
			setTimeout(() => {
				resolve();
			}, 100)
		});
	},
	debouncedCalculateTotalPricesUsingPRE: function () {
		let cart = null;
		ConnectivitySolutionPlugin.debounceF(
			() => ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE().then(c => cart = c),
			500
		);

		return Promise.resolve(cart);
	},
	calculateTotalPricesUsingPRE: async function calculateTotalPricesUsingPRE() {
		console.log('Calculating prices...');

		const version = '3-2-0';
		const version_1 = '3-1-0';
		const addOnAttributeName = 'SKU';
		// const productCodeAtt = 'Product Code';
		// const childCodeAtt = 'Add On Code';
		// const childGroupAtt = 'Group Name';

		function getCommonCartSpec(id, pricingRuleGroupCodes) {
			return {
				version: version,
				id: id,
				pricingRuleGroupCodes: pricingRuleGroupCodes || []
			}
		}

		function getChildItem(childConfig){
			let productCode = childConfig.configuration.getAttribute('add on code').value;
			let quantityValue = childConfig.configuration.getAttribute(ConnectivitySolutionConstants.quantity).value;
			return {
				version: version_1,
				id: childConfig.guid,
				quantity: Number(quantityValue) || 1,
				// insert Add on Code of addon here (SKU)
				catalogueItemId: productCode
			}
		}

		function toCartItem(config) {
			// perhaps a better mechanism of fetching sku/code?
			const catalogueItemAttr = Object.values(config.attributes).find(attr => attr.name === addOnAttributeName && attr.value);
			const catalogueItemId = catalogueItemAttr ? catalogueItemAttr.value : undefined;
			const quantityAttr = Object.values(config.attributes).find(attr => attr.name === ConnectivitySolutionConstants.quantity && attr.value);
			const quantity = quantityAttr ? quantityAttr.value : 1;
			
			if (catalogueItemId) {
				let catalogItem = {
					version: version_1,
					id: config.guid, // check if use something else
					quantity: Number(quantity) || 1, // should always be 1 in solution console
					catalogueItemId: catalogueItemId
				};
				if(config.relatedProductList && config.relatedProductList.length > 0){
					console.log('config.relatedProductList',config.relatedProductList)
					catalogItem.childItems = getCartChildItems(config);
				}
				return catalogItem;
			}
		}
			

		function getCartChildItems(config){
			let childItems = {};
			config.relatedProductList.forEach(function (item){
				let groupName = item.groupName;
				if(childItems.hasOwnProperty(groupName)){
					childItems[groupName].push( getChildItem(item));
				} else {
					childItems[groupName] = [getChildItem(item)];
				}

			});
			return childItems;
		}

			/*const catalogueItemId = 'ATT-D-3-N-01500-00256-D-XX';
			let xattributes = [];
			let attribute = {};
			attribute.id = "ContractTerm";
			attribute.name = "ContractTerm";
			attribute.value = '12';
			xattributes.push(attribute);

			if (catalogueItemId) {
				return {
					version: version_1,
					id: config.guid, // check if use something else
					quantity: 1, // should always be 1 in solution console
					catalogueItemId: catalogueItemId,
					attributes: xattributes
				}
			}*/

		function uuidv4() {
			return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
				(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
			);
		}

		function setTotalSolutionValue(totalOneOff, totalRecurring) {
			function financial(x) {
				return Number.parseFloat(x).toFixed(2);
			}

			// console.log('solution -' ,solution)
			const contractTerm = ConnectivitySolutionPlugin.getSmAttributeValue(
				solution, ConnectivitySolutionConstants.mainSolutionName,
				'Contract Term'
			);

			console.log('contract Term -' ,contractTerm)
			const total = totalOneOff + (contractTerm * totalRecurring);


			
			const totalValueElems = document.querySelectorAll('.pricing-table-header .price .total-value');
			const totalValueElem = totalValueElems && totalValueElems[0];

			totalValueElem && (totalValueElem.innerText = financial(total));
		}

		const solution = ConnectivitySolutionPlugin.solution;
		const pricingRuleGroupCodes = ConnectivitySolutionPlugin.getPricingRuleGroupsForSolution(solution);
		// if (!pricingRuleGroupCodes || pricingRuleGroupCodes.length === 0) {
		// 	return null;
		// }

		let configurations = ConnectivitySolutionPlugin.getSolutionConfigurations(solution);
		// console.log('configurations',configurations)
		// let components = Object.values(solution.components);
		// console.log('components',components)
		// let components = Object.values(solution.components);
		// let commonCart;
		
		// 	const mainConfig = Object.values(solution.schema.configurations)[0];
		// 	const catalogueItemAttr = Object.values(mainConfig.attributes).find(attr => attr.name === productCodeAtt && attr.value).value;
		// 	const mainConfigGuid = solution.getConfigurations()['0'].guid;
		// 	commonCart = {
		// 		...getCommonCartSpec(uuidv4(), pricingRuleGroupCodes),
		// 		items: [
		// 		             {
		// 		             	id: mainConfigGuid,
		// 		             	quantity:1,
		// 		             	version: version,
		// 		             	catalogueItemId: catalogueItemAttr,		
		// 		             	childItems: components.reduce((items,component)=> (
		// 		             		items[component.name] = 
		// 		             		[...Object.values(component.getConfigurations()).map(toCartItem).filter(Boolean)],items), {}
		// 		             	)
		// 		             }
		// 		]
		// 	};
		const commonCart = {
			...getCommonCartSpec(uuidv4(), pricingRuleGroupCodes),
			items: [
				...configurations.map(toCartItem).filter(Boolean)
			]
		};
		console.log(commonCart);

		/*let configurations = ConnectivitySolutionPlugin.getSolutionConfigurations(solution);
		const commonCart = {
			...getCommonCartSpec(uuidv4(), pricingRuleGroupCodes,[...configurations.map(toCartItem).filter(Boolean)])
		};
		console.log(commonCart);*/

	
		const start = performance.now();
		console.log('PREApiVnnnPricings request:', commonCart);
		//const result = await ConnectivitySolutionPlugin.performRemoteAction('PREApiV4Pricings', { payload: JSON.stringify(commonCart) });
		const result = await ConnectivitySolutionPlugin.performRemoteAction('CS_PREApiV6Pricings', { payload: JSON.stringify(commonCart) });
		console.log('PREApiVnnnPricings result:', result);
		const end = performance.now();
		console.log('Request time: ' + (end - start).toFixed(2) + 'ms');
		const body = JSON.parse(result.body);
		console.info('Received:', body);

		// if PRE returns actions
		if (body.actions && body.actions.length > 0) {
			await ConnectivitySolutionPlugin.invokePREActions(body.actions);

			// update PRE cart items to set the correct (solution config) guid. Check for a better solution!
			configurations = ConnectivitySolutionPlugin.getSolutionConfigurations(solution);
			configurations = configurations.filter(c => !c.attributes.ismaincomponent || !c.attributes.ismaincomponent.value);
			const allGuids = configurations.map(c => c.guid);
			const allPreGuids = body.cart.items.map(i => i.id);
			const diffs = allGuids.filter(g => !allPreGuids.includes(g));

			body.cart.items.forEach(i => {
				if (!allGuids.includes(i.id)) {
					i.id = diffs[0];
				}
			});
		}

		/* The following code should be separate, not a part of the calculation mechanism */

		await ConnectivitySolutionPlugin.attachDiscountsAndChargesToConfigurations(ConnectivitySolutionPlugin.solution, body.cart, true);
		await ConnectivitySolutionPlugin.attachDiscountsAndChargesToChildConfigurations(ConnectivitySolutionPlugin.solution, body.cart);

		// TODO: is this really necessary, can we do without this
		CS.currentPRECart = body.cart;

		console.log('Pricing totals:', body.cart.pricing);

		setTotalSolutionValue(body.cart.pricing.totalSalesOneOffPrice, body.cart.pricing.totalSalesRecurringPrice);

		console.log('CS.drawSidebar start' + new Date().toISOString());
		await CS.drawSidebar(body.cart);
		console.log('CS.drawSidebar done ' + new Date().toISOString());

		return body.cart;
	},
	invokePREActions: async function (actions) {
		// performing remote calls for each action returned by PRE to get the correct commercial product values
		// for the new configuration (since we only add new configs for now and check only for that action).
		// Currently only one action is being returned so that doesn't cause problems but might be necessary to
		// find a better solution at some point.
		for (const a of actions) {
			if (a.payload && a.type === 'addToCart') {
				const result = await ConnectivitySolutionPlugin.performRemoteAction(
					'CS_CommercialProductPRELookup',
					{sku: a.payload.sku}
				);

				const {commercialProduct, componentName} = result;

				const contractTerm = ConnectivitySolutionPlugin.getSmAttributeValue(
					ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName,
					'Contract Term'
				);

				console.log('contract Term -' ,contractTerm)

				const attributes = [
					{name: 'CommercialProduct', value: {value: commercialProduct.Id, displayValue: commercialProduct.Name}},
					{name: 'SKU', value: {value: a.payload.sku, displayValue: a.payload.sku}},
					{name: 'Config Name', value: {value: commercialProduct.Name, displayValue: commercialProduct.Name}},
					{name: 'Contract Term', value: {value: contractTerm, displayValue: contractTerm}}
				];

				const component = await ConnectivitySolutionPlugin.solution.getComponentByName(componentName);

				component && await ConnectivitySolutionPlugin.addNewConfiguration(component, attributes);
			}
		}
	},
	addNewConfiguration: async function (component, attributeList) {
		const config = component.createConfiguration(attributeList);
		return await component.addConfiguration(config);
	},
	getPricingRuleGroupsForSolution: function getPricingRuleGroupsForSolution(solution) {
		const configurations = solution.schema.configurations && Object.values(solution.schema.configurations);
		const cataloguePRGs = Object.values(configurations[0].attributes).map(it => {
			if (it.name === ConnectivitySolutionConstants.catalogueCodeAttributeName || it.name === ConnectivitySolutionConstants.promoCodeAttributeName) {
				return it.value;
			}
			return null;
		});

		return cataloguePRGs.filter(Boolean);
	},
	attachDiscountsAndChargesToChildConfigurations: async function attachDiscountsAndChargesToChildConfigurations(solution, currentPRECart) {

	    console.log('attachDiscountsAndChargesToChildConfigurations', currentPRECart);

		const payload = {};
		const attrUpdateArray = [];

		const allConfigs = ConnectivitySolutionPlugin.getSolutionConfigurations(solution);
		
			for (const packageIItem of currentPRECart.items) {
				
				for (const compConfigs of Object.values(packageIItem.childItems)) {
					for (const cartItem of compConfigs) {
						const childConfig = allConfigs.find(config => config.guid === cartItem.id);
						// console.log('childConfig',childConfig)
						// console.log('cartItem',cartItem)


						const stringifiedAddonDiscounts = JSON.stringify({
							discounts: cartItem.pricing.discounts
							.map(ConnectivitySolutionPlugin.makeDiscountReadOnlyForDealManagementPackage)
							.map(ConnectivitySolutionPlugin.copySourceToDescription)
						});
						//let config = solution.getConfigurationById(configId);
						// let childConfig =config.relatedProducts[addon.id];
						payload[childConfig.guid] = stringifiedAddonDiscounts;
						const retVal = await ConnectivitySolutionPlugin.setSmAttributeValueByGuid(
							childConfig.guid, ConnectivitySolutionConstants.preDiscountsAttributeName, stringifiedAddonDiscounts, 'return'
						);

						const retValRecurring = await ConnectivitySolutionPlugin.setSmAttributeValueByGuid(
							childConfig.guid, ConnectivitySolutionConstants.childRecurringAttributeName, cartItem.pricing.salesRecurringPrice, 'update'
						);
						const retValOneOff = await ConnectivitySolutionPlugin.setSmAttributeValueByGuid(
							childConfig.guid, ConnectivitySolutionConstants.childOneOffAttributeName, cartItem.pricing.salesOneOffPrice, 'update'
						);

						attrUpdateArray.push(retVal);
						// childConfig.updateAttribute(ConnectivitySolutionConstants.preDiscountsAttributeName,{
						// 	displayValue: stringifiedAddonDiscounts,
						// 	value: stringifiedAddonDiscounts
						// });
						// childConfig.updateAttribute(ConnectivitySolutionConstants.recurringAttributeName,{
						// 	displayValue: cartItem.pricing.salesRecurringPrice,
						// 	value: cartItem.pricing.salesRecurringPrice
						// });
						// childConfig.updateAttribute(ConnectivitySolutionConstants.oneOffAttributeName,{
						// 	displayValue: cartItem.pricing.salesOneOffPrice,
						// 	value: cartItem.pricing.salesOneOffPrice
						// });
						// if(cartItem.childItems){
						// 	Object.keys(cartItem.childItems).forEach(function(key){
						// 		for(const addon of cartItem.childItems[key]){
						// 			console.log('addon attachDiscountsAndChargesToChildConfigurations ', addon)
						// 			const stringifiedAddonDiscounts = JSON.stringify({
						// 				discounts: addon.pricing.discounts
						// 					.map(ConnectivitySolutionPlugin.makeDiscountReadOnlyForDealManagementPackage)
						// 					.map(ConnectivitySolutionPlugin.copySourceToDescription)
						// 			});
						// 			//let config = solution.getConfigurationById(configId);
						// 			let childConfig =config.relatedProducts[addon.id];
						// 			childConfig.configuration.updateAttribute(ConnectivitySolutionConstants.preDiscountsAttributeName,{
						// 				displayValue: stringifiedAddonDiscounts,
						// 				value: stringifiedAddonDiscounts
						// 			});
						// 			childConfig.configuration.updateAttribute(ConnectivitySolutionConstants.recurringAttributeName,{
						// 				displayValue: addon.pricing.salesRecurringPrice,
						// 				value: addon.pricing.salesRecurringPrice
						// 			});
						// 			childConfig.configuration.updateAttribute(ConnectivitySolutionConstants.oneOffAttributeName,{
						// 				displayValue: addon.pricing.salesOneOffPrice,
						// 				value: addon.pricing.salesOneOffPrice
						// 			});
									
						// 		}
						// 	});
						// }


					}
				}
			}
			
			if (attrUpdateArray && attrUpdateArray[0]){
				const componentToUpdateMapMap = attrUpdateArray.reduce((acc, curr) => {
					if (!acc[curr.componentName]) {
						acc[curr.componentName] = {};
					}
					acc[curr.componentName] = Object.assign(acc[curr.componentName], curr.updateMap);
					return acc;
				}, {});
				
				for (let componentName of Object.keys(componentToUpdateMapMap)) {
					const updateRes = await ConnectivitySolutionPlugin.updateConfigurationAttributesForComponent(componentName, componentToUpdateMapMap[componentName]);
					console.log(updateRes);
				}
				
				return  ConnectivitySolutionPlugin.performRemoteAction('CS_PREConfigurationUpdater', payload);
			}
			else
				return;
	},
	attachDiscountsAndChargesToConfigurations: async function attachDiscountsAndChargesToConfigurations(solution, currentPRECart, skipRemoteAction) {
		const allConfigs = ConnectivitySolutionPlugin.getSolutionConfigurations(solution);
		const payload = {};
		const attrUpdateArray = [];

		for (const cartItem of currentPRECart.items) {
			const guid = cartItem.id;
			const discounts = cartItem.pricing.discounts;

			const config = allConfigs.find(config => config.guid === guid);
			const configId = config && config.id;

			const stringifiedDiscounts = JSON.stringify({
				discounts: discounts
					.map(ConnectivitySolutionPlugin.makeDiscountReadOnlyForDealManagementPackage)
					.map(ConnectivitySolutionPlugin.copySourceToDescription)
			});

			payload[configId] = stringifiedDiscounts;

			const retVal = await ConnectivitySolutionPlugin.setSmAttributeValueByGuid(
				guid, ConnectivitySolutionConstants.preDiscountsAttributeName, stringifiedDiscounts, 'return'
			);

			const retValRecurring = await ConnectivitySolutionPlugin.setSmAttributeValueByGuid(
				guid, ConnectivitySolutionConstants.recurringAttributeName, cartItem.pricing.salesRecurringPrice, 'update'
			);
			const retValOneOff = await ConnectivitySolutionPlugin.setSmAttributeValueByGuid(
				guid, ConnectivitySolutionConstants.oneOffAttributeName, cartItem.pricing.salesOneOffPrice, 'update'
			);

			attrUpdateArray.push(retVal);

			// TODO: get and set the PRG Codes to attributes
		}

		const componentToUpdateMapMap = attrUpdateArray.reduce((acc, curr) => {
			if (!acc[curr.componentName]) {
				acc[curr.componentName] = {};
			}
			acc[curr.componentName] = Object.assign(acc[curr.componentName], curr.updateMap);
			return acc;
		}, {});

		for (let componentName of Object.keys(componentToUpdateMapMap)) {
			const updateRes = await ConnectivitySolutionPlugin.updateConfigurationAttributesForComponent(componentName, componentToUpdateMapMap[componentName]);
			// console.log(updateRes);
		}

		return !skipRemoteAction ? ConnectivitySolutionPlugin.performRemoteAction('CS_PREConfigurationUpdater', payload) : Promise.resolve(true);
	},
	makeDiscountReadOnlyForDealManagementPackage: function makeDiscountReadOnlyForDealManagementPackage(discount) {
		if (discount) {
			discount.customData = Object.assign(discount.customData || {}, { promotion: true });
		}

		return discount;
	},
	copySourceToDescription: function copySourceToDescription(discount) {
		if (discount) {
			discount.description = discount.description || discount.source;
		}

		return discount;
	},
	setSmAttributeValue: async function setSmAttributeValue(solution, configName, attributeName, attributeValue, returnOrUpdate) {
		const solutionConfigurations = ConnectivitySolutionPlugin.getSolutionConfigurations(solution);

		//c.configurationName has a space, therefor we trim it..
		const config = solutionConfigurations.find(c => c.configurationName.trim() === configName);

		config && await ConnectivitySolutionPlugin.setSmAttributeValueByGuid(config.guid, attributeName, attributeValue, returnOrUpdate);
	},
	setSmAttributeValues: async function setSmAttributeValues(solution, attributeName, attributeValue, returnOrUpdate) {
		const solutionConfigurations = ConnectivitySolutionPlugin.getSolutionConfigurations(solution);

		// console.log('attributeName',attributeName)
		// console.log('attributeValue',attributeValue)
		return Promise.all(solutionConfigurations.map(async (config) => {
			return await ConnectivitySolutionPlugin.setSmAttributeValueByGuid(config.guid, attributeName, attributeValue, returnOrUpdate);
		}));
	},
	setSmAttributeValueByGuid: async function setSmAttributeValueByGuid(configGuid, attributeName, attributeValue, returnOrUpdate) {
		if (!returnOrUpdate) {
			returnOrUpdate = 'update';
		}

		const updateMap = {
			[configGuid]: [{
				name: attributeName, value: attributeValue, displayValue: attributeValue
			}]
		};

		
		const component = ConnectivitySolutionPlugin.findComponentByConfigGuid(ConnectivitySolutionPlugin.solution, configGuid);
		const componentName = component && component.name;
		
		
		if(component){
			if (returnOrUpdate === 'return') {
				return {
					componentName: componentName,
					updateMap: updateMap
				};
			} else if (returnOrUpdate === 'update') {
				return ConnectivitySolutionPlugin.updateConfigurationAttributesForComponent(componentName, updateMap);
			}
		}
	},
	findComponentByConfigGuid: function findComponentByConfigGuid(solution, configGuid) {
		for (const component of Object.values(solution.components)) {
			for (const configuration of Object.values(component.schema.configurations)) {
				if (configuration.guid === configGuid) {
					return component;
				}
			}
		}

		for (const configuration of Object.values(solution.schema.configurations)) {
			if (configuration.guid === configGuid) {
				return solution;
			}
		}

		return null;
	},

	 setAttributeValue: function setAttributeValue (componentName, config, attributeName, value) {
		var updateMap = {};
		updateMap[config.guid] = [
			{name: attributeName, lookup: "", value: value, displayValue: value}
		];
		return CS.SM.getActiveSolution().then(
			async function (solution) {
				return solution.getComponentByName('Access').updateConfigurationAttribute(config.guid, updateMap[config.guid]);
			});
	},

	resetAllAttributes: function resetAllAttributes(componentName, config, updateMap)
	{
		return CS.SM.getActiveSolution().then(
			async function (solution) {
				return solution.getComponentByName('Access').updateConfigurationAttribute(config.guid, updateMap[config.guid]);
			});
	},

	getDefaultPRGAndFa: async function getDefaultPRGAndFa() {
		
		//const result = await ConnectivitySolutionPlugin.performRemoteAction('FAM_Data_Retriever', { basketId: await CS.SM.getActiveBasketId() });
		const result = await ConnectivitySolutionPlugin.performRemoteAction('CS_FAM_Data_Retriever', { basketId: this.basketId });

		const attributeValues = [
			ConnectivitySolutionPlugin.getSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'AccountId'),
			ConnectivitySolutionPlugin.getSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'Catalogue'),
			ConnectivitySolutionPlugin.getSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'Frame Agreement'),
			ConnectivitySolutionPlugin.getSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, ConnectivitySolutionConstants.catalogueCodeAttributeName),
			ConnectivitySolutionPlugin.getSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'Frame Agreement PRGs'),
		];

		if (result.accountId && !attributeValues[0]) await ConnectivitySolutionPlugin.setSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'AccountId', result.accountId);
		if (result.catalogueName && !attributeValues[1]) await ConnectivitySolutionPlugin.setSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'Catalogue', result.catalogueName);
		if (result.faName && !attributeValues[2]) await ConnectivitySolutionPlugin.setSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'Frame Agreement', result.faName);

		if (result.selectedPRG && !attributeValues[3]) await ConnectivitySolutionPlugin.setSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, ConnectivitySolutionConstants.catalogueCodeAttributeName, result.selectedPRG);
		if (result.selectedFA && !attributeValues[4]) await ConnectivitySolutionPlugin.setSmAttributeValue(ConnectivitySolutionPlugin.solution, ConnectivitySolutionConstants.mainSolutionName, 'Frame Agreement PRGs', result.selectedFA);
	},
	attachPRGsToBasket: async function attachPRGsToBasket(cart) {
		let localCart;
		if (!cart) {
			localCart = await ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE();
		} else {
			localCart = cart;
		}

		const allPrgs = localCart && localCart.pricingRuleGroupCodes || [];

		// CS.SM.session.basketId was used before! Didn't work at the time of writing, but might get fixed.
		const payload = {
			[this.basketId]: allPrgs.filter(Boolean).join(',')
		};

		return ConnectivitySolutionPlugin.performRemoteAction('CS_PREBasketUpdater', payload);
	},
	updatePlugin: function(Plugin) {
		if (Plugin.name === ConnectivitySolutionConstants.mainSolutionName) {
			_updateConnectivitySolutionPlugin(Plugin);
		}
	},
};

function _updateConnectivitySolutionPlugin(Plugin) {
	/**
	 * Provides the user with an opportunity to do something once the attribute is updated.
	 *
	 * @param {string} component
	 * @param {object} attribute - The attribute which is being updated.
	 * @param {string} beforeAttr - Before change value.
	 */
	Plugin.afterAttributeUpdated = async function (component, configuration, attribute, beforeAttr) {
		const afterValue = attribute && attribute.value;
		const beforeValue = beforeAttr && beforeAttr.value;
		// CPQ-11 technical component changes
		/*
		async function addTechnicalComponentsByCPId(CPId){
			inputMap = {};
			inputMap["CPId"] = CPId;
			console.log({inputMap})
			let result = await ConnectivitySolutionPlugin.performRemoteAction('CS_GetTechnical_Components', inputMap);
			console.log({result})
			const oeComponent = Object.values(component.orderEnrichments).find(oe => oe.name==='Technical Component')
			result["Technical Components"].forEach(async (TC) => {
				let configMap = [{name: "Name", value: {value: TC.cspmb__material__r.Name}, readOnly: true},{name: "SKU", value: { value: TC.cspmb__material__r.cspmb__code__c}},{name: "Type", value: { value: TC.cspmb__material__r.cspmb__type__c}}];
				let orderEnrichmentConfiguration = oeComponent.createConfiguration(configMap);
				await component.addOrderEnrichmentConfiguration(configuration.guid, orderEnrichmentConfiguration, false);
			});
		}

		async function deleteTechnicalComponents(CPId){
			inputMap = {};
			inputMap["CPId"] = CPId;
			console.log({inputMap})
			let result = await ConnectivitySolutionPlugin.performRemoteAction('CS_GetTechnical_Components', inputMap);
			console.log({result})
			const skus = await result["Technical Components"].map((TC) => 
				TC.cspmb__material__r.cspmb__code__c
			);
			// const oeConfigs = component.getOrderEnrichmentConfigurationList([configuration.guid])
			const techCompList = Object.values(configuration.getOrderEnrichments()).map(OeMap=>Object.values(OeMap).filter(OE=>OE.configurationName.startsWith('Technical Component'))).flat()
			console.log(techCompList)
			console.log({skus})
			skus.forEach(async (sku) => {
				const oeConfigGuid = techCompList.find(OEconfig => OEconfig.getAttribute('sku')?.value === sku)?.guid
				console.log({oeConfigGuid})
				await component.deleteOrderEnrichmentConfiguration(configuration.guid, oeConfigGuid, false);
			})
		}
		*/
		if (afterValue === beforeValue) {
			return Promise.resolve(true);
		}
		let solution = ConnectivitySolutionPlugin.solution;

		if (!solution) {
			try {
				ConnectivitySolutionPlugin.solution = await CS.SM.getActiveSolution();
				solution = ConnectivitySolutionPlugin.solution;
			} catch(e) {
				console.error('Failed loading active solution!', e);
			}
		}

		if (component.name === ConnectivitySolutionConstants.mainSolutionName) {
			if (attribute && attribute.name === 'Contract Term') {
				await ConnectivitySolutionPlugin.propagateContractTermToComponents();

				return await ConnectivitySolutionPlugin.wait()
					.then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE)
					.then(ConnectivitySolutionPlugin.attachPRGsToBasket);
			}
		}

		if (component.name === ConnectivitySolutionConstants.accessComponentName) {

			// if (attribute.name === 'Product Family Value') {
			// 	updateMap = {};
			// 	updateMap[configuration.guid] = [
			// 		{name: 'product group', lookup: "", value: "", displayValue: ""},
			// 		{name: 'product group value', lookup: "", value: "", displayValue: ""},
			// 	];
			// 	return ConnectivitySolutionPlugin.resetAllAttributes(component, configuration,updateMap);

			// } else if (attribute.name === 'Product Group Value') {
			// 	updateMap = {};
			// 	updateMap[configuration.guid] = [
			// 		{name: 'product type', lookup: "", value: "", displayValue: ""},
			// 		{name: 'product type value', lookup: "", value: "", displayValue: ""}
			// 	];
			// 	return ConnectivitySolutionPlugin.resetAllAttributes(component, configuration,updateMap);
			// } else if (attribute.name === 'Product Type Value') {
			// 	updateMap = {};
			// 	updateMap[configuration.guid] = [
			// 		{name: 'access carrier', lookup: "", value: "", displayValue: ""},
			// 		{name: 'access carrier value', lookup: "", value: "", displayValue: ""}
			// 	];
			// 	return ConnectivitySolutionPlugin.resetAllAttributes(component, configuration,updateMap);
			// } else if (attribute.name === 'Access carrier Value') {
			// 	updateMap = {};
			// 	updateMap[configuration.guid] = [
			// 		{name: 'commercialproductid', lookup: "", value: "", displayValue: ""}
			// 	];
			// 	return ConnectivitySolutionPlugin.resetAllAttributes(component, configuration,updateMap);
			// } else if (attribute.name === 'Product Group') {
			// 	return ConnectivitySolutionPlugin.setAttributeValue(component, configuration, 'product group value', attribute.displayValue);
			// } else if (attribute.name === 'Access Carrier') {
			// 	return ConnectivitySolutionPlugin.setAttributeValue(component, configuration, 'access carrier value', attribute.displayValue);
			// } else if (attribute.name === 'Product Type') {
			// 	return ConnectivitySolutionPlugin.setAttributeValue(component, configuration, 'product type value', attribute.displayValue);
			// } 

			/*let oeConfig;
			if (configuration.orderEnrichmentList) {
				configuration.orderEnrichmentList.forEach((oe) => {
					if (oe.name.includes('Access Address')) {
						oeConfig = oe;
					}
					else
						return;
				});
			}
			if (oeConfig.attributes) {
				console.log('fayaz==' + oeConfig.attributes.address.value);
			}

			console.log('fayaz==' + oeConfig);*/
			
		}

		// hide/show the deal management button if a frame agreement is selected
		if (attribute && attribute.name === 'Frame Agreement' && afterValue) {
			// document.querySelectorAll('#redirectToDealManagement').forEach(elem => { elem.style.display = 'none' })
		} else if (attribute && attribute.name === 'Frame Agreement' && !afterValue) {
			// document.querySelectorAll('#redirectToDealManagement').forEach(elem => { elem.style.display = 'initial' })
		}
		// console.log('attribute', {attribute})
		// CPQ-11 technical component changes
		/*
		if (attribute && attribute.name === 'Advanced Security Value') {
			const advSecurityCPEAttr = configuration.getAttribute('Adv Security CPE')
			if (attribute.value == 'No'){
				advSecurityCPEAttr.readOnly = false
			}
			// else if (attribute.value == 'Yes'){
			// 	advSecurityCPEAttr.readOnly = true
			// }
			else if (attribute.value == ''){
				deleteTechnicalComponents(advSecurityCPEAttr.value)
				advSecurityCPEAttr.readOnly = true
				advSecurityCPEAttr.value = ""
				advSecurityCPEAttr.displayValue = ""
			}
		}	
		if (attribute && attribute.name === 'Adv Security CPE') {
			if (attribute.value !== '')
				addTechnicalComponentsByCPId(attribute.value)
			else if (attribute.value === '')
				deleteTechnicalComponents(beforeValue)
		}
		if (attribute && attribute.name === 'CommercialProductID' && attribute.value === '') {
			if (component.name === ConnectivitySolutionConstants.SDWANComponentName){
				deleteTechnicalComponents(beforeValue)
			} 
		}
		*/
		
		if (attribute && attribute.name === 'SKU' && attribute.value != '') {

			if (component.name === ConnectivitySolutionConstants.accessComponentName)
			{			
				inputMap = {};
				inputMap["contractTerm"] = configuration.getAttribute('Contract Term').value;
				inputMap["cpID"] = configuration.getAttribute('CommercialProductID').value;		
				let result = await ConnectivitySolutionPlugin.performRemoteAction('CS_AccessRelatedProducts', inputMap);
				
				const commercialProductProfessionalInstall = result['Professional Install'][0]
				const accessProfessionalInstallAttributeList = [{name:"Instal Type", value: { value: "Fusion Instal", displayValue: "Fusion Instal" }},
				{name:"add on code", value: { value: commercialProductProfessionalInstall.cspmb__Price_Item_Code__c, displayValue: commercialProductProfessionalInstall.cspmb__Price_Item_Code__c }},
				{name:"CommercialProductID", value: { value: commercialProductProfessionalInstall.Id, displayValue: commercialProductProfessionalInstall.Name }},
				{name:"NRC", value: { value: commercialProductProfessionalInstall.cspmb__One_Off_Charge__c || 0, displayValue: commercialProductProfessionalInstall.cspmb__One_Off_Charge__c || 0 }},
				{name:"MRC", value: { value: commercialProductProfessionalInstall.cspmb__recurring_charge__c || 0, displayValue: commercialProductProfessionalInstall.cspmb__recurring_charge__c || 0 }}]
				const accessProfessionalInstallRelatedProduct = component.createRelatedProduct('AddOn Access Professional Install', accessProfessionalInstallAttributeList)
				await component.addRelatedProduct( configuration.guid, accessProfessionalInstallRelatedProduct )
				
				const commercialProductDevice = result['Device'][0]
				const accessDeviceAttributeList = [{name:"Purchase Type", value: { value: "Rental", displayValue: "Rental" }},
				{name:"Device Type", value: { value: commercialProductDevice.fc_cpe_type__c, displayValue: commercialProductDevice.fc_cpe_type__c }},
				{name:"Device Type Value", value: { value: commercialProductDevice.fc_cpe_type__c, displayValue: commercialProductDevice.fc_cpe_type__c }},
				{name:"add on code", value: { value: commercialProductDevice.cspmb__Price_Item_Code__c, displayValue: commercialProductDevice.cspmb__Price_Item_Code__c }},
				{name:"CommercialProductID", value: { value: commercialProductDevice.Id, displayValue: commercialProductDevice.Name }},
				{name:"NRC", value: { value: commercialProductDevice.cspmb__One_Off_Charge__c || 0, displayValue: commercialProductDevice.cspmb__One_Off_Charge__c || 0 }},
				{name:"MRC", value: { value: commercialProductDevice.cspmb__recurring_charge__c || 0, displayValue: commercialProductDevice.cspmb__recurring_charge__c || 0 }}]
				const accessDeviceRelatedProduct = component.createRelatedProduct('AddOn Access Device', accessDeviceAttributeList)
				await component.addRelatedProduct( configuration.guid, accessDeviceRelatedProduct )
				
			}
            await ConnectivitySolutionPlugin.wait()
			.then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE)
			.then(ConnectivitySolutionPlugin.attachPRGsToBasket);
			
			// CPQ-11 technical component changes
			/*
			if (component.name === ConnectivitySolutionConstants.SDWANComponentName) {			
				const CPId = configuration.getAttribute('commercialproductid').value
				deleteTechnicalComponents(CPId)
				addTechnicalComponentsByCPId(CPId,)
			}
			*/
			
		} else if (
			(attribute && attribute.name === ConnectivitySolutionConstants.catalogueCodeAttributeName ||
			attribute && attribute.name === ConnectivitySolutionConstants.promoCodeAttributeName) &&
			afterValue
		) {
			await ConnectivitySolutionPlugin.wait()
				.then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE)
				.then(ConnectivitySolutionPlugin.attachPRGsToBasket);
		} else if (attribute && attribute.name === 'Frame Agreement PRGs') {
			await ConnectivitySolutionPlugin.wait()
				.then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE)
				.then(ConnectivitySolutionPlugin.attachPRGsToBasket);
		}

		return Promise.resolve(true);
	};

	/**
	 * Hook executed after the configuration is added via the UI add configuration button
	 *
	 * @param {string} componentName
	 * @param {Object} configuration - Configuration object with attributes to be set
	 */
	Plugin.afterConfigurationAdd = async function (componentName, configuration) {
		// console.log('afterConfigurationAdd', componentName, configuration);

		// await ConnectivitySolutionPlugin.debouncedCalculateTotalPricesUsingPRE()
		// 	.then(this.attachDiscountsAndChargesToConfigurations);

		await ConnectivitySolutionPlugin.propagateContractTermToComponents();

		/*await ConnectivitySolutionPlugin.wait()
			.then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE)
			.then(ConnectivitySolutionPlugin.attachPRGsToBasket);*/

		return Promise.resolve(true);
	};

	/**
	 * Hook executed after the configuration is deleted via the UI delete configuration button
	 *
	 * @param {string} componentName
	 * @param {Object} configuration - Configuration object which was deleted
	 */
	Plugin.afterConfigurationDelete = async function (componentName, configuration) {
		// console.log('afterConfigurationDelete', componentName, configuration);

		// await ConnectivitySolutionPlugin.debouncedCalculateTotalPricesUsingPRE()
		// 	.then(this.attachDiscountsAndChargesToConfigurations);

		await ConnectivitySolutionPlugin.wait()
			.then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE)
			.then(ConnectivitySolutionPlugin.attachPRGsToBasket);

		return Promise.resolve(true);
	};

	/**
	 * Hook executed after the configuration is cloned via the UI clone configuration button
	 *
	 * @param {string} component
	 * @param {Object} configurations - Configurations which were cloned
	 */
	Plugin.afterConfigurationClone = async function (component, configurations) {
		console.log('afterConfigurationClone', component, configurations);

		await ConnectivitySolutionPlugin.propagateContractTermToComponents();

		// await ConnectivitySolutionPlugin.debouncedCalculateTotalPricesUsingPRE()
		// 	.then(this.attachDiscountsAndChargesToConfigurations);

		await ConnectivitySolutionPlugin.wait()
			.then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE)
			.then(ConnectivitySolutionPlugin.attachPRGsToBasket);

		return Promise.resolve(true);
	}

	/**
	 * Hook executed before the configuration is added via the UI add configuration button
	 *
	 * @param {string} componentName
	 * @param {Object} configuration - Empty configuration object with attributes to be set
	 */
	Plugin.beforeOrderEnrichmentConfigurationAdd = function (
		componentName,
		configuration,
		orderEnrichmentConfiguration
	) {
		console.log('beforeOrderEnrichmentConfigurationAdd', componentName,
			configuration,
			orderEnrichmentConfiguration);
		return Promise.resolve(true);
	};

	/**
	 * Hook executed after the configuration is added via the UI add configuration button
	 *
	 * @param {string} componentName
	 * @param {Object} configuration - Configuration object with attributes to be set
	 */
	Plugin.afterOrderEnrichmentConfigurationAdd = function (
		componentName,
		configuration,
		orderEnrichmentConfiguration
	) {
		console.log('afterOrderEnrichmentConfigurationAdd', componentName,
			configuration,
			orderEnrichmentConfiguration);
		return Promise.resolve(true);
	};

	/**
	 * Hook executed before the configuration is deleted via the UI delete configuration button
	 *
	 * @param {string} componentName
	 * @param {Object} configuration - Configuration object which is going to be deleted
	 */
	Plugin.beforeOrderEnrichmentConfigurationDelete = function (
		componentName,
		configuration,
		orderEnrichmentConfiguration
	) {
		console.log('beforeOrderEnrichmentConfigurationDelete', componentName, configuration, orderEnrichmentConfiguration);
		// if (orderEnrichmentConfiguration?.getAttribute('type')?.value === "Technical SKU"){
		// 	CS.SM.displayMessage('Can not delete default Technical Components', 'error');
		// 	return Promise.reject();
		// } else
			return Promise.resolve(true);
	};

	/**
	 * Hook executed after the configuration is deleted via the UI delete configuration button
	 *
	 * @param {string} componentName
	 * @param {Object} configuration - Configuration object which was deleted
	 */
	Plugin.afterOrderEnrichmentConfigurationDelete = function (
		componentName,
		configuration,
		orderEnrichmentConfiguration
	) {
		console.log('afterOrderEnrichmentConfigurationDelete', componentName,
			configuration,
			orderEnrichmentConfiguration);
		return Promise.resolve(true);
	};

	/**
	 * Hook executed before we save the complex product to SF. We need to resolve the promise as a
	 * boolean. The save will continue only if the promise resolve with true.
	 *
	 * @param {Object} complexProduct
	 */
	Plugin.beforeSave = function (complexProduct, configurationsProcessed, saveOnlyAttachment) {
		//console.log('Before save', complexProduct);
			return ConnectivitySolutionPlugin.attachPRGsToBasket().then(() => {
			return Promise.resolve(true);
		});
	};

	/**
	 * Hook executed before we save the complex product to SF. We need to resolve the promise as a
	 * boolean. The save will continue only if the promise resolve with true.
	 *
	 * @param {Object} complexProduct
	 */
	Plugin.afterSave = async (complexProduct, configurationsProcessed, saveOnlyAttachment) => {
		ConnectivitySolutionPlugin.solution = complexProduct.solution;
		/*	
		let addrArray = [];
		Object.values(ConnectivitySolutionPlugin.solution.getAllConfigurations()).forEach(config=>
		{
			const orderEnrichments = Object.values(config.getOrderEnrichments())
			if (orderEnrichments.length)
			addrArray.push(...Object.values(orderEnrichments[0]))
		})
		const uniqueAddresses = new Set(addrArray.map(config=>config.attributes['address name']?.value))
		const response  = await ConnectivitySolutionPlugin.performRemoteAction (
			'CS_CheckSEApproval' , { 'siteCount': uniqueAddresses.size, 'solutionId': ConnectivitySolutionPlugin.solution.id}
		)
		*/
	};
         //return Promise.resolve(true);
		
		// return ConnectivitySolutionPlugin.wait()
			//.then(attachDiscountsAndChargesToConfigurations(true))
			//.then(wait)
			//.then(calculateTotalPricesUsingPRE);
	

	/**
	 * Hook executed before we save the complex product to SF. We need to resolve the promise as a
	 * boolean. The save will continue only if the promise resolve with true.
	 *
	 * Note: Seems that this hook is not working as expected!
	 * Calling CS.SM.getActiveSolution() immediately after this hook doesn't return the solution.
	 * Using SolutionSetActive event listener instead!
	 *
	 * @param {Object} complexProduct
	 */
	// Plugin.afterSolutionLoaded = function (previousComplexProduct, loadedComplexProduct) {
	// 	return ConnectivitySolutionPlugin.afterSolutionLoaded(loadedComplexProduct);
	// };

	/**
	 * Hook executed before the  Related product is added to the configuration
	 *
	 * @param {string} componentName
	 * @param {Object} configuration
	 */
	Plugin.beforeRelatedProductAdd = function (componentName, configuration, beforeRelatedProductAdd) {
		console.log('beforeRelatedProductAdd',componentName,  configuration, beforeRelatedProductAdd);
		if(beforeRelatedProductAdd.relatedProductName === 'AddOn SDWAN'){
			componentName.getComponentByName(componentName.name).getAddonsForConfiguration(configuration.guid).then(function(addOns){
				// console.log('++++++ component name ' + componentName.name);
				let addOnAttribute = beforeRelatedProductAdd.configuration.getAttribute('AddOn SDWAN');
				Object.keys(addOns).forEach(function(key){
					addOns[key].allAddons.forEach(function (addOn) {
						if(addOnAttribute.value === addOn.Id) {
							beforeRelatedProductAdd.configuration.updateAttribute('Add On Code', {
								displayValue: addOn.cspmb__Add_On_Price_Item__r.cspmb__Add_On_Price_Item_Code__c,
								value: addOn.cspmb__Add_On_Price_Item__r.cspmb__Add_On_Price_Item_Code__c
							});
							// beforeRelatedProductAdd.configuration.updateAttribute('Group Name', {
							// 	displayValue: addOn.cspmb__Group__c,
							// 	value: addOn.cspmb__Group__c
							// });
						}
					});
				});

			} ).then(function () {
				ConnectivitySolutionPlugin.wait().then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE);
			});


		}
		else if(beforeRelatedProductAdd.relatedProductName === 'AddOn Access'){
			componentName.getComponentByName(componentName.name).getAddonsForConfiguration(configuration.guid).then(function(addOns){
				// console.log('++++++ component name ' + componentName.name);
				let addOnAttribute = beforeRelatedProductAdd.configuration.getAttribute('addon access');
				Object.keys(addOns).forEach(function(key){
					addOns[key].allAddons.forEach(function (addOn) {
						if(addOnAttribute.value === addOn.Id) {
							beforeRelatedProductAdd.configuration.updateAttribute('Add On Code', {
								displayValue: addOn.cspmb__Add_On_Price_Item__r.cspmb__Add_On_Price_Item_Code__c,
								value: addOn.cspmb__Add_On_Price_Item__r.cspmb__Add_On_Price_Item_Code__c
							});
							// beforeRelatedProductAdd.configuration.updateAttribute('Group Name', {
							// 	displayValue: addOn.cspmb__Group__c,
							// 	value: addOn.cspmb__Group__c
							// });
						}
					});
				});

			} ).then(function () {
				ConnectivitySolutionPlugin.wait().then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE);
			});
		}
		else if(beforeRelatedProductAdd.relatedProductName === 'AddOn Access Device' || beforeRelatedProductAdd.relatedProductName === 'AddOn Access Professional Install'){
			console.log({configuration})
			const contractTerm = configuration.getAttribute('Contract Term').value
			beforeRelatedProductAdd.configuration.updateAttribute('Contract Term', {
				displayValue: Number(contractTerm),
				value: Number(contractTerm)
			});
		}
		
		return Promise.resolve(true);
	};

	/**
	 * Hook executed after the related product is added to the configuration
	 *
	 * @param {string} componentName
	 * @param {Object} configuration
	 */
	Plugin.afterRelatedProductAdd = function (componentName, configuration, relatedProduct) {
		ConnectivitySolutionPlugin.wait().then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE);
		console.log('afterRelatedProductAdd', configuration, relatedProduct);
		return Promise.resolve(true);
	};

	Plugin.beforeRelatedProductDelete = function (componentName, configuration, relatedProduct) {

		console.log('beforeRelatedProductDelete', configuration, relatedProduct);
		return Promise.resolve(true);
	};

	Plugin.afterRelatedProductDelete = function (componentName, configuration, relatedProduct) {
		ConnectivitySolutionPlugin.wait().then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE);
		console.log('afterRelatedProductDelete', configuration, relatedProduct);
		return Promise.resolve(true);
	};

	Plugin.afterQuantityUpdated = function ( component, configuration, oldQuantity, newQuantity ) {
		ConnectivitySolutionPlugin.wait().then(ConnectivitySolutionPlugin.calculateTotalPricesUsingPRE);
		console.log('afterQuantityUpdated', component, configuration, oldQuantity, newQuantity);
		return Promise.resolve(true);
	};

	Plugin.beforeSiteProductAdd = function (componentName, configuration) {
		console.log('beforeSiteProductAdd');
		return Promise.resolve(true);
	};

	Plugin.afterSiteProductAdd = function (componentName, configuration, siteProduct) {
		console.log('afterSiteProductAdd', configuration, siteProduct);
		return Promise.resolve(true);
	};

	Plugin.beforeSiteProductDelete = function (componentName, configuration, siteProduct) {

		console.log('beforeSiteProductDelete', configuration, siteProduct);
		return Promise.resolve(true);
	};

	Plugin.afterSiteProductDelete = function (componentName, configuration, siteProduct) {

		console.log('afterSiteProductDelete', configuration, siteProduct);
		return Promise.resolve(true);
	};

	/**
	 * Hook to handle custom button click event
	 *
	 * @param {string} componentName
	 * @param {Object} buttonSettings setting from the json schema for the button
	 */
	Plugin.buttonClickHandler = function (buttonSettings) {

		if (buttonSettings.id === 'backToBasketInternal') {
			return Promise.resolve('/' + CS.SM.session.basketId);
		} else if (buttonSettings.id === 'redirectToDealManagement') {
			return Promise.resolve('/apex/csdiscounts__DiscountPage?basketId=' + CS.SM.session.basketId);
		}
		return Promise.resolve(true);
	};

	/**
	 * Hook before active component is changed
	 *
	 * @param {Object} component
	 * @param {Object} fileData
	 */
	Plugin.beforeNavigate = function (currentComponent, previousComponent) {

		return Promise.resolve({ allow: true });
	};

	/**
	 * Hook to alter alert message
	 *
	 * @param {string} message
	 * @param {string} type
	 */
	Plugin.alterAlertMessage = function (message, type) {

		return Promise.resolve({ message, type });
	};
}