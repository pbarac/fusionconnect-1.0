trigger CS_AllCommercialProductTriggers on cspmb__Price_Item__c (before insert, before update,after insert,after update) {
    
    No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
            
    if (noTriggers == null || !noTriggers.flag__c) {           
        if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
             CS_TriggerHandler.execute(new CS_AllCommercialProductTriggerDelegate(Trigger.New));
        }                
    }
}