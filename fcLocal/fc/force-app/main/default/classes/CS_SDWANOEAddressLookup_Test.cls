/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANOEAddressLookup_Test - Test coverage for class CS_SDWANOEAddressLookup
*/
@isTest 
public class CS_SDWANOEAddressLookup_Test{

    @isTest public static void doLookupSearchTest(){
        cscrm__Address__c addr = CS_TestDataFactory.createAddress(true);
        Account acc = CS_TestDataFactory.createAccount(true);
        List<cscfga__Product_Basket__c> pbLst = CS_TestDataFactory.createBaskets(1, acc.Id, true);
        
        Map<String,String> searchFields = new Map <String, String>();
        searchFields.put('basketId', pbLst[0].Id);

        Test.startTest();
        CS_SDWANOEAddressLookup testObject = new CS_SDWANOEAddressLookup();
        testObject.getRequiredAttributes();        
        Object[] addrLst = testObject.doLookupSearch(searchFields,null,null,null,null);
        Test.stopTest();
        
        System.assertNotEquals(addrLst.size(), null, 'Prioritized Bandwidth Values are not fetched');
    }
}