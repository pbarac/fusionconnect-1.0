/**
* Developed by CloudSense LTD, London (UK)
* @description CS_SDWANOEAddressLookup - SDWAN Address order enrichment look up for address in solution management
*/
global class CS_SDWANOEAddressLookup extends cscfga.ALookupSearch {
    
    public static final String REQUIRED_ATTRIBUTES = '["Id", "Name", "basketId"]';

    // Attribute names used in the WHERE clause of SOQL query, JSON notation, e.g. '["Attribute1","Attribute2"]'
    public override String getRequiredAttributes() {
        return REQUIRED_ATTRIBUTES;
    }    

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
        List<cscrm__Address__c> addressLst = new List<cscrm__Address__c>();
        String basketId = searchFields.get('basketId');
        
        cscfga__Product_Basket__c pb = [Select id, csordtelcoa__Account__c from cscfga__Product_Basket__c where id =: basketId];
        
        if(pb != null && pb.csordtelcoa__Account__c != null){
            addressLst = [SELECT id, name, Full_Address__c, cscrm__Building_Number__c, cscrm__Street__c, cscrm__Unit_Number__c, cscrm__City__c, cscrm__State_Province__c, cscrm__Zip_Postal_Code__c, cscrm__Country__c FROM  cscrm__Address__c
                            WHERE cscrm__Account__c =: pb.csordtelcoa__Account__c];
                            
        }
        return addressLst;
    }

}